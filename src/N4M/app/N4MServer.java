/**
 * File Name  :     N4MServer
 * Version    :     1.0
 * Author     :     Ruolin Wang
 * Date       :     02/26/2018
 * Function   :     This class provides the method to support N4M query                  
 * CopyRight Reserved
 * 
 */

package N4M.app;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import N4M.serialization.N4MException;
import N4M.serialization.N4MMessage;
import N4M.serialization.N4MQuery;
import N4M.serialization.N4MResponse;
import N4M.serialization.ApplicationEntry;
import N4M.serialization.ErrorCodeType;

/**
 * Class Name  :     N4MServer
 * @author Ruolin Wang
 *
 */
public class N4MServer {

    private DatagramPacket receivePacket = null;
    private DatagramSocket udpSocket = null;
    private long timestamp = 0L;
    private Map<String, Integer> applications = null;
    private Logger logger = null; // Server logger

    /**
     * 
     * Create N4MServer
     * @param N4Mlogger the server logger
     * @param receivePacket received packet
     * @param udpSocket udp socket
     * @param timestamp timestamp
     * @param applications applications map
     */
    public N4MServer(Logger N4Mlogger, DatagramPacket receivePacket, DatagramSocket udpSocket, long timestamp,
            Map<String, Integer> applications) {
        this.receivePacket = receivePacket;
        this.udpSocket = udpSocket;
        this.timestamp = timestamp;
        this.applications = applications;
        this.logger = N4Mlogger;
    }

    /**
     * 
     * Handle the query
     * @throws IOException
     * @throws N4MException
     */
    public void handleProcess() throws IOException, N4MException {

        //decode the received message
        byte[] encodedReceivedMessage = Arrays.copyOfRange(receivePacket.getData(), 0, receivePacket.getLength());
        N4MMessage receivedMessage = null;
        try {
            receivedMessage = N4MMessage.decode(encodedReceivedMessage);
        } catch (N4MException e) {

            //send different error message according to the exception
            switch (e.getErrorCodeType()) {
            case INCORRECTHEADER: {
                sendErrorResponse(udpSocket, ErrorCodeType.INCORRECTHEADER, 0);
                break;
            }
            case BADMSGSIZE: {
                sendErrorResponse(udpSocket, ErrorCodeType.BADMSGSIZE, 0);
                break;
            }
            case BADMSG: {
                sendErrorResponse(udpSocket, ErrorCodeType.BADMSG, 0);
                break;
            }
            default:
                sendErrorResponse(udpSocket, ErrorCodeType.SYSTEMERROR, 0);
                break;
            }
        }
        // if received N4MResponse Message
        if ((receivedMessage instanceof N4MResponse)) {
            sendErrorResponse(udpSocket, ErrorCodeType.BADMSG, receivedMessage.getMsgId());
        }

        if ((receivedMessage instanceof N4MQuery)) {
            // log the received query message
            logger.log(Level.INFO, "Query: " + receivePacket.getAddress().toString() + ":" + receivePacket.getPort()
                    + " " + ((N4MQuery) receivedMessage).toString() + System.lineSeparator());

            // send the right response message
            List<ApplicationEntry> applicationEntries = new ArrayList<ApplicationEntry>();
            for (String key : applications.keySet()) {
                applicationEntries.add(new ApplicationEntry(key, applications.get(key)));
            }

            N4MResponse responseMessage = new N4MResponse(ErrorCodeType.NOERROR, receivedMessage.getMsgId(),
                    this.timestamp, applicationEntries);

            byte[] encodedResponseMessage = responseMessage.encode();
            udpSocket.send(new DatagramPacket(encodedResponseMessage, encodedResponseMessage.length,
                    receivePacket.getAddress(), receivePacket.getPort()));

            // log the response message
            logger.log(Level.INFO, "Response:" + " " + (responseMessage).toString() + System.lineSeparator());
        }

    }

    private void sendErrorResponse(DatagramSocket udpSocket, ErrorCodeType type, int MsgId)
            throws N4MException, IOException {
        // log the error to the log file
        logger.log(Level.WARNING,
                "Error received packet. The error type is " + type.toString() + System.lineSeparator());
        // send the error response to client
        N4MResponse responseMessage = new N4MResponse(type, MsgId, 0, new ArrayList<ApplicationEntry>());
        byte[] encodedQueryMessage = responseMessage.encode();
        udpSocket.send(new DatagramPacket(encodedQueryMessage, encodedQueryMessage.length, receivePacket.getAddress(),
                receivePacket.getPort()));
    }
}
