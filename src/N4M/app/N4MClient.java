/**
 * File Name  :     N4MClient
 * Version    :     1.0
 * Author     :     Ruolin Wang
 * Date       :     03/22/2018
 * Function   :     Implement an N4M client using UDP
 *                   
 * CopyRight Reserved
 * 
 */
package N4M.app;

import java.net.DatagramSocket;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.Random;

import N4M.serialization.N4MException;
import N4M.serialization.N4MMessage;
import N4M.serialization.N4MQuery;
import N4M.serialization.N4MResponse;

import java.io.IOException;
import java.io.InterruptedIOException;

/**
 * Class Name  :     N4MClient
 * Author      :     Ruolin Wang
 * Function    :     Implement an N4M client using UDP
 * 
 */
public class N4MClient {
    private static final int TIMEOUT = 3000; // Resend timeout (milliseconds)
    private static final int MAXTRIES = 5; // Maximum retransmissions
    private static final int MAXMSGID = 255; // Maximum msgId
    private static final int MAXDPLENGTH = 65535;// Maximum datagramPacket length

    /**
     * @param args Parameter(s): <Server Name/IP> <Server Port> <Business Name>
     * @throws IOException didn't get anything
     * @throws N4MException Invalid message
     */
    public static void main(String[] args) throws IOException, N4MException {

        if ((args.length != 3)) { // Test for correct # of args
            throw new IllegalArgumentException("Parameter(s): <Server Name/IP> <Server Port> <Business Name> ");
        }
        InetAddress serverAddress = InetAddress.getByName(args[0]); // Server address
        int servPort;
        try {
            servPort = Integer.parseInt(args[1]); // Server port 
        } catch (Exception e) {
            throw new IllegalArgumentException(
                    "Parameter(s): <Server Name/IP> <Server Port> <Business Name>. Port must be an integer. ");
        }
        DatagramSocket socket = new DatagramSocket();
        socket.setSoTimeout(TIMEOUT); // Maximum receive blocking time (milliseconds)
        socket.connect(serverAddress, servPort);

        // Create a query message (3nd param is the business name)
        Random rand = new Random();
        int msgId = rand.nextInt(MAXMSGID);
        N4MQuery queryMessage = new N4MQuery(msgId, args[2]);
        // encode queryMessage and send it
        byte[] encodedQueryMessage = queryMessage.encode();
        DatagramPacket message = new DatagramPacket(encodedQueryMessage, encodedQueryMessage.length);

        // Receiving packet
        DatagramPacket receivePacket = new DatagramPacket(new byte[MAXDPLENGTH], MAXDPLENGTH);

        N4MResponse responseMessage = null;
        int tries = 0; // Packets may be lost, so we have to keep trying

        boolean receivedResponse = false;
        boolean isWrongMsgId = false;
        do {
            socket.send(message); // Send the query message
            try {
                socket.receive(receivePacket); // Attempt reply reception

                if (!receivePacket.getAddress().equals(serverAddress)) {// Check source
                    //throw new IOException("Received packet from an unknown source");
                    System.err.println("Not from the server");
                    receivedResponse = false;
                    tries += 1;
                }

                //decode the received message
                byte[] encodedReceivedMessage = Arrays.copyOfRange(receivePacket.getData(), 0,
                        receivePacket.getLength());
                N4MMessage receivedMessage = N4MMessage.decode(encodedReceivedMessage);

                if (!(receivedMessage instanceof N4MResponse)) {
                    receivedResponse = false;
                    tries = MAXTRIES;
                } else {
                    responseMessage = (N4MResponse) receivedMessage;
                    if (responseMessage.getMsgId() != msgId) {
                        receivedResponse = false;
                        isWrongMsgId = true;
                        tries = MAXTRIES;
                    } else {
                        receivedResponse = true;
                    }
                }

            } catch (InterruptedIOException e) { // We did not get anything
                tries += 1;
                System.err.println("Timed out, " + (MAXTRIES - tries) + " more tries...");
            }
        } while ((!receivedResponse) && (tries < MAXTRIES));

        if (receivedResponse) {
            System.out.println(responseMessage.toString());
        } else {
            if (isWrongMsgId) {
                System.err.println("Wrong Msg Id");
            } else {
                System.err.println("No response or invalid response");
            }

        }
        socket.close();
    }

}
