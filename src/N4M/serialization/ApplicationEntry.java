/**
 * File Name  :     ApplicationEntry
 * Version    :     1.0
 * Author     :     Ruolin Wang
 * Date       :     03/22/2018
 * Function   :     This class represents one application and its access count
 *                   
 * CopyRight Reserved
 * 
 */
package N4M.serialization;

import java.util.Objects;

/**
 * Class Name  :     ApplicationEntry
 * Author      :     Ruolin Wang
 * Function    :     This class represents one application and its access count
 * 
 */
public class ApplicationEntry {

    private int accessCount = 0; // access count
    private String applicationName = null; // application name
    private final int MAXAPPLICATIONCOUNT = 65535; // the maximum application count, use 16 bit to store so that 2^16
    private final int MAXAPPLICATIONLENGTH = 255; // the maximum name length of applications, use 8 bit to store

    /**
     * Creates application entry
     * @param applicationName name of application
     * @param accessCt application access count
     * @throws N4MException 
     * @throws NullPointerException 
     */
    public ApplicationEntry(String applicationName, int accessCt) throws N4MException,NullPointerException {
        setAccessCount(accessCt);
        setApplicationName(applicationName);
    }
    
  
    /**
     * Return human-readable representation
     * @return human-readable representation
     */
    @Override
    public String toString() {
        return "Application: " + applicationName + " Access Count: " + accessCount;
    }

    /**
     * Return application access count
     * @return access count
     */
    public int getAccessCount() {
        return accessCount;
    }

    /**
     * Set application access count
     * @param accessCount access count
     * @throws N4MException if validation fails
     * @throws NullPointerException 
     */
    public void setAccessCount(int accessCount) throws N4MException {
        Objects.requireNonNull(accessCount,
                "accessCt is null for ApplicationEntry(String applicationName, int accessCt)");
        /* if access count is smaller than zero or larger than MAXAPPLICATIONCOUNT throw N4MException*/          
        if (accessCount < 0 || accessCount > MAXAPPLICATIONCOUNT) { 
            throw new N4MException("Invalid access count", ErrorCodeType.BADMSG);
        }
        this.accessCount = accessCount;
    }

    /**
     * Returns application name
     * @return application name
     */
    public String getApplicationName() {
        return applicationName;
    }
    
    
    /**
     * Set application name
     * @param applicationName application name
     * @throws N4MException if validation fails
     * @throws NullPointerException if applicationName is null
     */
    public void setApplicationName(String applicationName) throws N4MException,NullPointerException {
        Objects.requireNonNull(applicationName,
                "applicationName is null for ApplicationEntry(String applicationName, int accessCt)");
        /*if applicationName length<0 or applicationName >MAXAPPLICATIONLENGTH or contains non-ascii characters*/
        if (applicationName.length() < 0 || applicationName.length() > MAXAPPLICATIONLENGTH
                || !isAllASCII(applicationName)) {
            throw new N4MException("Invalid application name", ErrorCodeType.BADMSG);
        }
        this.applicationName = applicationName;
    }
    
    /**
     *  Overrides equals in class java.lang.Object
     */
    @Override
    public boolean equals(Object obj) {
        boolean result = false;
        if (obj == null || obj.getClass() != this.getClass()) {
            result = false;
        } else {
            ApplicationEntry inApplicationEntry = (ApplicationEntry) obj;
            if (this.applicationName.equals(inApplicationEntry.applicationName)
                    && this.accessCount == inApplicationEntry.accessCount) {
                result = true;
            }

        }
        return result;
    }
    
    /**
     *  Overrides hashCode in class java.lang.Object
     */
    @Override
    public int hashCode() {
        return Objects.hash(applicationName) * 32 + Objects.hash(accessCount);
    }
    
    /**
     *  To judge if a string contains no non-ascii characters
     */
    private static boolean isAllASCII(String input) {
        boolean isASCII = true;
        for (int i = 0; i < input.length(); i++) {
            int c = input.charAt(i);
            if (c > 0x7F) {
                isASCII = false;
                break;
            }
        }
        return isASCII;
    }

}
