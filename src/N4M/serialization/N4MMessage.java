/**
 * File Name  :     N4MMessage
 * Version    :     1.0
 * Author     :     Ruolin Wang
 * Date       :     03/22/2018
 * Function   :     Represents generic portion of a N4M message and provides serialization/deserialization. 
 *                  This ONLY specifies an API (i.e., minimum required class/method names and parameter types). 
 *                  You may add any classes you wish. You may make any class/method abstract or concrete. 
 *                  You may add any methods (public, protected, or private) to these classes. 
 *                  You may override any methods in the subclasses.
 *                   
 * CopyRight Reserved
 * 
 */
package N4M.serialization;


import java.util.Objects;

/**
 * Class Name  :     N4MMessage
 * Author      :     Ruolin Wang
 * Function    :     Represents generic portion of a N4M message and provides serialization/deserialization. 
 *                   This ONLY specifies an API (i.e., minimum required class/method names and parameter types). 
 *                   You may add any classes you wish. You may make any class/method abstract or concrete. 
 *                   You may add any methods (public, protected, or private) to these classes. 
 *                   You may override any methods in the subclasses.
 * 
 */
public abstract class N4MMessage {

    protected ErrorCodeType errorCodeType = null; // error code type which is also used in sub class
    protected int msgId = 0; // msg id which is also used in sub class

    protected static final byte VERSION = (byte) 0b00100000; // the protocal version
    private static final byte VERSIONMASK = (byte) 0b11110000; // the version mask

    protected static final byte ERRCODEMASK = (byte) 0b00000111; // the error code mask to get the error code
    private static final byte MAXERRCODE = (byte) 0b00000100; // the maximum error code

    protected static final byte QRMASK = (byte) 0b00001000; // the QR mask to get the error code
    protected static final byte QUERYBIT = (byte) 0b00000000; // the bit indicates query
    protected static final byte RESPONSEBIT = (byte) 0b00001000; // the bit indicates response

    protected static final int HEADERLENGTH = 2; // the header length
    protected static final int MAXVALIDLENGTH = 255; // the max valid length in a byte

    /**
     * Creates a new N4M message by deserializing from the given byte array according to the specified serialization.
     * @param in buffer of received packet
     * @return new N4M message 
     * @throws N4MException if validation fails (e.g., bad version, incorrect packet size, etc.)
     * @throws NullPointerException if in is null
     */
    public static N4MMessage decode(byte[] in) throws N4MException,NullPointerException {
        Objects.requireNonNull(in, "in is null for N4MMessage decode(byte[] in) ");
        if (in.length < 3) { // the message doesn't have enough data
            throw new N4MException("Received packet contains no valid data", ErrorCodeType.BADMSGSIZE);
        }
        // analysis the header
        byte firstByteInHeader = in[0]; // get the first byte in header

        if ((firstByteInHeader & VERSIONMASK) != VERSION) { // incorrect version
            throw new N4MException("Incorrect Version", ErrorCodeType.INCORRECTHEADER);
        }
        if ((firstByteInHeader & ERRCODEMASK) > MAXERRCODE) { // incorrect error code
            throw new N4MException("Incorrect Error Code", ErrorCodeType.INCORRECTHEADER);
        }

        N4MMessage message = null; // create the message base on qr type
        if ((firstByteInHeader & QRMASK) == QUERYBIT) {
            message = new N4MQuery(in); // new query message
        } else {
            message = new N4MResponse(in); // new response message
        }
        return message;
    }

    /**
     * message encoded in byte array
     * @return encoded N4M message
     * 
     * 
     */
    public abstract byte[] encode();

    /**
     * Return human-readable representation
     * @return human-readable representation
     */
    @Override
    public abstract String toString();

    /**
     * Return error code number
     * @return error code number
     */
    public ErrorCodeType getErrorCode() {
        return this.errorCodeType;
    }

    /**
     * Set error code by code type
     * @param errorCodeType new error code type
     * @throws N4MException throw exception when query give a invalid error type
     * @throws NullPointerException 
     * 
     */
    public void setErrorCode(ErrorCodeType errorCodeType) throws N4MException,NullPointerException{
        Objects.requireNonNull(errorCodeType, "errorCodeType is null for setErrorCodeType(ErrorCodeType errorCodeType)");
        this.errorCodeType = errorCodeType;
    }

    /**
     * Return message ID
     * @return message ID
     */
    public int getMsgId() {
        return msgId;
    }

    /**
     * Set the message ID
     * @param msgId new message ID
     * @throws N4MException if validation fails
     * 
     */
    public void setMsgId(int msgId) throws N4MException {
        Objects.requireNonNull(msgId, "msgId is null for setMsgId(int msgId)");
        if (msgId > 255 || msgId < 0) {
            throw new N4MException("Invalid msgId", ErrorCodeType.BADMSG);
        }
        this.msgId = msgId;
    }

    /**
     *  Overrides equals in class java.lang.Object
     */
    @Override
    public abstract boolean equals(Object obj);

    /**
     *  Overrides hashCode in class java.lang.Object
     */
    @Override
    public abstract int hashCode();

}
