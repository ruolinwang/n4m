/**
 * File Name  :     N4MQuery
 * Version    :     1.0
 * Author     :     Ruolin Wang
 * Date       :     03/22/2018
 * Function   :     Represents an N4M query and provides serialization/deserialization
 *                   
 * CopyRight Reserved
 * 
 */
package N4M.serialization;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.util.Objects;

/**
 * Class Name  :     N4MQuery
 * Author      :     Ruolin Wang
 * Function    :     Represents an N4M query and provides serialization/deserialization
 * 
 */
public class N4MQuery extends N4MMessage {

    private String businessName = null;

    /**
     * The constructor used by N4MMessage.decode to create query message
     */
    protected N4MQuery(byte[] in) throws N4MException {
        int msgIdByteToInt = in[1] & 0xff; // 0xff is the mask to change the byte to unsigned byte
        int businessNameLength = in[2] & 0xff; // 0xff is the mask to change the byte to unsigned byte
        
        int errorCodeNumByteToInt = in[0] & ERRCODEMASK;
        
        if (businessNameLength < 0 || businessNameLength > MAXVALIDLENGTH) {
            throw new N4MException("Incorrect BusinessNameLength", ErrorCodeType.BADMSG);
        }
        if (in.length - HEADERLENGTH - 1 != businessNameLength) {
            throw new N4MException("Incorrect message length", ErrorCodeType.BADMSGSIZE);
        }
        String nameForBusiness = "";
        for (int i = 3; i < in.length; i++) {
            nameForBusiness += (char) (in[i] & 0xff);
        }

        setMsgId(msgIdByteToInt);
        setBusinessName(nameForBusiness);
        setErrorCode(ErrorCodeType.valueOf(errorCodeNumByteToInt));

    }

    /**
     * Creates a new N4M query using given values
     * @param msgId message ID
     * @param businessName business name
     * @throws N4MException if validation fails
     * @throws NullPointerException if input is null
     */
    public N4MQuery(int msgId, String businessName) throws N4MException,NullPointerException {
        setMsgId(msgId);
        setBusinessName(businessName);
        setErrorCode(ErrorCodeType.NOERROR);
    }

    /**
     * overrides the encode method in N4MMessage
     */
    @Override
    public byte[] encode() {
    	    try {
    	    	ByteArrayOutputStream byteStream = new ByteArrayOutputStream(); // to buffer the output bytes
    	        DataOutputStream out = new DataOutputStream(byteStream);
    	        int firstByte = this.getErrorCode().getErrorCodeNum() | QUERYBIT | VERSION;
    	        out.writeByte(firstByte);
    	        out.writeByte(this.getMsgId());
    	        out.writeByte(this.getBusinessName().length());
    	        out.writeBytes(this.getBusinessName());

    	        out.flush();
    	        byte[] data = byteStream.toByteArray();
    	        return data;
    	    } catch(Exception e) {
    	    	    System.err.println(e.getMessage());
    	    }
		return null;
        
    }

    /**
     * Return business name
     * @return business name
     */
    public String getBusinessName() {
        return businessName;
    }

    /**
     * Set business name
     * @param businessName business name
     * @throws N4MException if validation fails
     * @throws NullPointerException if business name is null
     */
    public void setBusinessName(String businessName) throws N4MException,NullPointerException {
        Objects.requireNonNull(businessName, "businessName is null for setBusinessName(String businessName)");
        if (!isAllASCII(businessName) || businessName.length() > 255) {
            throw new N4MException("Invalid businessName name", ErrorCodeType.BADMSG);
        }
        this.businessName = businessName;
    }

    /**
     * Overrides the setErrorCodeNum since for query the only code num is 0
     */
    @Override
    public void setErrorCode(ErrorCodeType errorCodeType) throws N4MException {
        Objects.requireNonNull(errorCodeType, "setErrorCodeType(int errorCodeType)");
        if (!errorCodeType.equals(ErrorCodeType.NOERROR) ) {
            throw new N4MException("Invalid error code number in query package", ErrorCodeType.INCORRECTHEADER);
        }
        this.errorCodeType = errorCodeType;
    }

    /**
     * Return human-readable representation
     * @return human-readable string
     */
    @Override
    public String toString() {
        return "Business Name: " + businessName + " Message ID: " + msgId;
    }

    /**
     *  Overrides:
     *   equals in class N4MMessage
     */
    @Override
    public boolean equals(Object obj) {
        boolean result = false;
        if (obj == null || obj.getClass() != this.getClass()) {
            result = false;
        } else {
            N4MQuery n4MQuery = (N4MQuery) obj;
            if (this.businessName.equals(n4MQuery.businessName) && this.msgId == n4MQuery.msgId
                    && this.errorCodeType.equals(n4MQuery.errorCodeType)) {
                result = true;
            }

        }
        return result;
    }

    /**
     *  Overrides:
     *   hashCode in class N4MMessage
     */
    @Override
    public int hashCode() {
        return Objects.hash(getErrorCode()) * 64 + Objects.hash(getMsgId()) * 32 + Objects.hash(getBusinessName());
    }

    /**
     *  To judge if a string contains no non-ascii characters
     */
    private static boolean isAllASCII(String input) {
        boolean isASCII = true;
        for (int i = 0; i < input.length(); i++) {
            int c = input.charAt(i);
            if (c > 0x7F) {
                isASCII = false;
                break;
            }
        }
        return isASCII;
    }

}
