/**
 * File Name  :     N4MResponse
 * Version    :     1.0
 * Author     :     Ruolin Wang
 * Date       :     03/22/2018
 * Function   :     Represents an N4M response and provides serialization/deserialization
 *                   
 * CopyRight Reserved
 * 
 */
package N4M.serialization;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Class Name  :     N4MResponse
 * Author      :     Ruolin Wang
 * Function    :     Represents an N4M response and provides serialization/deserialization
 * 
 */
public class N4MResponse extends N4MMessage {
	private long timestamp = (long) 0.0;
	private List<ApplicationEntry> applications = null;

	private static final long MAXTIMESTAMP = 4294967295L;
	private static final int STARTENTRYBYTE = 7;

	/**
	 * Represents an N4M response and provides serialization/deserialization
	 * @param errorCodeType errCodeType
	 * @param msgId message ID
	 * @param timestamp timestamp
	 * @param applications list of applications
	 * @throws N4MException if validation fails
	 * @throws NullPointerException if timestamp or applications is null
	 */
	public N4MResponse(ErrorCodeType errorCodeType, int msgId, long timestamp, List<ApplicationEntry> applications)
			throws N4MException, NullPointerException {

		setApplications(applications);
		setTimestamp(timestamp);
		setMsgId(msgId);
		setErrorCode(errorCodeType);
	}

	/**
	 * The constructor used by N4MMessage.decode to create response message
	 */
	protected N4MResponse(byte[] in) throws N4MException {
		if (in.length < 7) {
			throw new N4MException("Invalid response message", ErrorCodeType.BADMSGSIZE);
		}
		int msgIdByteToInt = in[1] & 0xff;
		int errorCodeNumByteToInt = in[0] & ERRCODEMASK;

		long timeSeconds = longFrom4Bytes(in, 2, false);
		long timeStampFromPacket = timeSeconds;

		int applicationCount = in[6] & 0xff;
		
		int entryByte = STARTENTRYBYTE;
		List<ApplicationEntry> appsFromPacket = new ArrayList<ApplicationEntry>();
		try {
			if (applicationCount != 0) {
				for (int i = 0; i < applicationCount; i++) {
					int applicationUseCountHigh = (in[entryByte] & 0xff) << 8;
					int applicationUseCountLow = (in[entryByte + 1] & 0xff);
					int applicationUseCount = applicationUseCountHigh | applicationUseCountLow;
					entryByte = entryByte + 2;

					int applicationNameNum = in[entryByte] & 0xff; // get the applicationNameNum
					String tempApplicationName = "";
					for (int j = 0; j < applicationNameNum; j++) {
						entryByte++;
						tempApplicationName += (char) (in[entryByte] & 0xff);
					}

					appsFromPacket.add(new ApplicationEntry(tempApplicationName, applicationUseCount));
					entryByte++;
				}
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new N4MException("Message is too short", ErrorCodeType.BADMSGSIZE);
		}
		
		if(in.length!=entryByte) {
			throw new N4MException("Message is too long", ErrorCodeType.BADMSGSIZE);
		}

		setTimestamp(timeStampFromPacket);
		setMsgId(msgIdByteToInt);
		setErrorCode(ErrorCodeType.valueOf(errorCodeNumByteToInt));
		setApplications(appsFromPacket);
	}

	/**
	 * overrides the encode method in N4MMessage
	 */
	@Override
	public byte[] encode() {
		try {
			ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
			DataOutputStream out = new DataOutputStream(byteStream);
			int firstByte = this.getErrorCode().getErrorCodeNum() | RESPONSEBIT | VERSION;
			out.writeByte(firstByte);
			out.writeByte(this.getMsgId());
			long timeStampSeconds = getTimestamp(); // get the timstamp and change it to seconds
			out.writeInt((int) timeStampSeconds);
			out.writeByte(this.getApplications().size());
			if (this.getApplications().size() != 0) {
				for (int i = 0; i < this.getApplications().size(); i++) {
					out.writeShort((short) this.getApplications().get(i).getAccessCount());
					out.writeByte(this.getApplications().get(i).getApplicationName().length());
					if (this.getApplications().get(i).getApplicationName().length() != 0) {
						out.writeBytes(this.getApplications().get(i).getApplicationName());
					}
				}
			}

			out.flush();
			byte[] data = byteStream.toByteArray();
			return data;
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return null;

	}

	/**
	 * Return human-readable representation
	 */
	@Override
	public String toString() {
		String output = "Timestamp: " + new Date(timestamp * 1000) + " Message ID: " + msgId + " Error Type: "
				+ errorCodeType.toString() + " Application Count: " + applications.size() + " Application list: "
				+ System.lineSeparator();
		if (applications.size() > 0) {
			for (int i = 0; i < applications.size(); i++) {
				output = output + applications.get(i).toString() + System.lineSeparator();
			}
		}

		return output;
	}

	/**
	 * Return timestamp
	 * @return timestamp
	 */
	public long getTimestamp() {
		return this.timestamp;

	}

	/**
	 * Set timestamp
	 * @param timestamp Time of last application execution, given in seconds since 1/1/1970. If no application has been executed then time should be 0.
	 * @throws N4MException if validation fails
	 * @throws NullPointerException 
	 */
	public void setTimestamp(long timestamp) throws N4MException, NullPointerException {
		Objects.requireNonNull(timestamp, "timestamp is null for setTimestamp(Date timestamp)");
		if (timestamp > MAXTIMESTAMP) { // if timestamp is in the future or
																				// larger than the MAXTIMESTAMP
			throw new N4MException("Invalid timestamp", ErrorCodeType.BADMSG);
		}
		this.timestamp = timestamp;
	}

	/**
	 * Get list of applications
	 * @return list of applications
	 */
	public List<ApplicationEntry> getApplications() {
		return applications.stream().map(t -> {
			try {
				return new ApplicationEntry(t.getApplicationName(), t.getAccessCount());
			} catch (N4MException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return t;
		}).collect(Collectors.toList());
	}

	/**
	 * Set the list of applications
	 * @param applications list of applications
	 * @throws N4MException too many applications
	 * @throws NullPointerException if applications is null
	 */
	public void setApplications(List<ApplicationEntry> applications) throws N4MException,NullPointerException {
		Objects.requireNonNull(applications,
				"applications is null for setApplications(List<ApplicationEntry> applications)");
		if (applications.size() > 255) {
			throw new N4MException("Too many applications", ErrorCodeType.BADMSG);
		}
		this.applications = applications.stream().map(t -> {
			try {
				return new ApplicationEntry(t.getApplicationName(), t.getAccessCount());
			} catch (N4MException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return t;
		}).collect(Collectors.toList());
		;
	}

	/**
	 *  Overrides:
	 *   equal in class N4MMessage
	 */
	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if (obj == null || obj.getClass() != this.getClass()) {
			result = false;
		} else {
			N4MResponse n4MResponse = (N4MResponse) obj;
			if (this.timestamp == n4MResponse.timestamp && this.msgId == n4MResponse.msgId
					&& this.errorCodeType.equals(n4MResponse.errorCodeType)
					&& this.applications.equals(n4MResponse.applications)) {
				result = true;
			}

		}
		return result;
	}

	/**
	 *  Overrides:
	 *   hashCode in class N4MMessage
	 */
	@Override
	public int hashCode() {
		return Objects.hash(getApplications()) * 128 + Objects.hash(getErrorCode()) * 64 + Objects.hash(getMsgId()) * 32
				+ Objects.hash(getTimestamp());
	}

	private long longFrom4Bytes(byte[] input, int offset, boolean littleEndian) {
		long value = 0;
		for (int count = 0; count < 4; ++count) {
			int shift = (littleEndian ? count : (3 - count)) << 3;
			value |= ((long) 0xff << shift) & ((long) input[offset + count] << shift);
		}
		return value;
	}

}
