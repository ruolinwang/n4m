/**
 * File Name  :     ErrorCodeType
 * Version    :     1.0
 * Author     :     Ruolin Wang
 * Date       :     03/22/2018
 * Function   :     Error Code enumerated type
 *                   
 * CopyRight Reserved
 * 
 */
package N4M.serialization;

/**
 * Class Name  :     ErrorCodeType
 * Author      :     Ruolin Wang
 * Function    :     Error Code enumerated type
 * 
 */
public enum ErrorCodeType {

    /**
     * No error
     */
    NOERROR(0),
    /**
     *  Incorrect header
     */
    INCORRECTHEADER(1),
    /**
     *  Message is too short or too long
     */
    BADMSGSIZE(2),
    /**
     *  Invalid message format
     */
    BADMSG(3),
    /**
     *  system error
     */
    SYSTEMERROR(4);

    private final int codeNum;

    /**
     * The private constructor to give an error code number
     */
    private ErrorCodeType(int codeNum) {
        this.codeNum = codeNum;
    }

    /**
     * Return error code number corresponding to the error code
     * @return error code number
     */
    public int getErrorCodeNum() {
        return this.codeNum;
    }

    /**
     * Return error code corresponding to the error code number
     * @param errorCodeNum error code number to find
     * @return corresponding error code
     * @throws N4MException if invalid error code number
     */
    public static ErrorCodeType valueOf(int errorCodeNum) throws N4MException {
        for (ErrorCodeType code : ErrorCodeType.values()) { // if the code num is in the enum 
            if (code.getErrorCodeNum() == errorCodeNum) {
                return code;
            }
        }
        throw new N4MException("invalid error code number", ErrorCodeType.INCORRECTHEADER); //otherwise throw exception
    }
}
