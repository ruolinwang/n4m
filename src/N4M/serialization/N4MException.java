/**
 * File Name  :     N4MException
 * Version    :     1.0
 * Author     :     Ruolin Wang
 * Date       :     03/22/2018
 * Function   :     N4M validation exception
 *                   
 * CopyRight Reserved
 * 
 */
package N4M.serialization;

/**
 * Class Name  :     N4MException
 * Author      :     Ruolin Wang
 * Function    :     N4M validation exception
 * 
 */
public class N4MException extends Exception {

    private static final long serialVersionUID = 1L;
    private ErrorCodeType errorCodeType;

    /**
     * Constructs N4M validation exception
     * @param msg exception message (cannot be null)
     * @param errorCodeType type of error(cannot be null)
     * @param cause exception cause
     * @throws NullPointerException if msg or errorCodeType is null
     */
    public N4MException(String msg, ErrorCodeType errorCodeType, Throwable cause) {
        super(msg, cause);
        if (errorCodeType == null) {
            throw new NullPointerException("errorCodeType is null" + msg);
        }
        this.errorCodeType = errorCodeType;
    }

    /**
     * Constructs N4M validation exception
     * @param msg exception message (cannot be null)
     * @param errorCodeType type of error(cannot be null)
     * @throws NullPointerException if msg or errorCodeType is null
     */
    public N4MException(String msg, ErrorCodeType errorCodeType) {
        this(msg, errorCodeType, null);
    }

    /**
     * Return error code type
     * @return error code type
     */
    public ErrorCodeType getErrorCodeType() {
        return errorCodeType;
    }
}
