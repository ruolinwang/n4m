/**
 * File Name  :     G8RLogFormatter
 * Version    :     1.0
 * Author     :     Ruolin Wang
 * Date       :     02/26/2018
 * Function   :     This class provides a self defined formatter
 *                   
 * CopyRight Reserved
 * 
 */
package G8R.app;

import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 * Class Name  :     G8RLogFormatter
 * @author Ruolin Wang
 *
 */
public class G8RLogFormatter extends Formatter{

    @Override
    public String format(LogRecord record) { // only show the message
        return formatMessage(record);
    }

}
