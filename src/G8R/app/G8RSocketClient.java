/**
 * File Name  :     G8RSocketClient
 * Version    :     1.0
 * Author     :     Ruolin Wang
 * Date       :     02/12/2018
 * Function   :     This class provides basic methods for communication via socket following G8R protocol
 *                   
 * CopyRight Reserved
 * 
 */


package G8R.app;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import G8R.serialization.CookieList;
import G8R.serialization.G8RMessage;
import G8R.serialization.G8RRequest;
import G8R.serialization.G8RResponse;
import G8R.serialization.MessageInput;
import G8R.serialization.MessageOutput;
import G8R.serialization.ValidationException;

/**
 * 
 * Class Name: G8RSocketClient
 * @author Ruolin Wang
 *
 */
public class G8RSocketClient {
    private static String function = null; //to store G8R function part
    private static String status = null; // to store G8R response status
    private static String[] params = null; // to store G8R request params
    private static CookieList cookieList = null; // to store G8R cookieList part
    private static Socket socket = null; // to store the socket interface
    private static MessageInput in = null; // reference to a MessageInput object to buffer the socket inputStream
    private static MessageOutput out = null; // reference to a MessageOutput object to buffer the socket outputStream
    private static FileInputStream fis = null; // reference to a file input stream
    private static FileOutputStream fos = null; // reference to a file output stream
    private static File file = null; //reference to a file
    private static MessageInput inFile = null; // reference to a MessageInput object to buffer the file inputStream
    private static MessageOutput outFile = null; // reference to a MessageOutput object to buffer the file outputStream
    private static Scanner sc = null; // scanner to read input

    /**
     * Creates a new G8RSocketClient using given values
     * @param server Server address
     * @param servPort Port number of server service
     * @param cookiesFileName the file name to store cookies
     * @throws UnknownHostException if host is unknown
     * @throws IOException if open file fails or build Stream fails
     */
    public G8RSocketClient(String server, Integer servPort, String cookiesFileName)
            throws UnknownHostException, IOException {
        // Create socket that is connected to server on specified port
        socket = new Socket(server, servPort);
        in = new MessageInput(socket.getInputStream());
        out = new MessageOutput(socket.getOutputStream());

        // Open the file and create FileInputStream
        file = new File(cookiesFileName);
        if (!file.exists()) { // if file doesn't exist, create the file
            file.createNewFile();
        }
        fis = new FileInputStream(file);
        inFile = new MessageInput(fis);

        // Read the cookieList. If the file is empty, create an empty cookieList
        try {
            cookieList = new CookieList(inFile);
        } catch (Exception e) {
            // to solve the situation that input file is empty
            if ("read the EOF of inputstream with no cookie in the list".equals(e.getMessage())) { 
                cookieList = new CookieList();
            } else {
                printError("Cookie File has error:" + e.getMessage());
            }
        }

        // Create FileOutputStream to write new cookieList into file
        fos = new FileOutputStream(file);
        outFile = new MessageOutput(fos);

        // Create scanner to get the input from console
        sc = new Scanner(System.in);
    }
    
    /**
     * To run the socket and communicate with server
     * @throws Exception when network or file I/O violation happens 
     */
    public void runClient() throws Exception {
        // To store previous valid message. When error happens, show the message as prompt
        String preValidMessage = null;
        // send the function with no params which come from user's input
        sendInputFunction();
        // Implement the communication with server in the main loop
        while (true) {
            // Decode the message received from the server
            G8RMessage responseMessage = G8RMessage.decode(in);
            // Terminate the program when receiving a message which is not a response message
            if (!(responseMessage instanceof G8RResponse)) {
                printError("Return is not a response message, close the connection");
            }
            // Get the status and check if it is "OK"
            status = ((G8RResponse) responseMessage).getStatus();

            // Get the cookieList and update the orginal cookieList
            CookieList tempCookieList = ((G8RResponse) responseMessage).getCookieList();
            for (String item : tempCookieList.getNames()) {
                cookieList.add(item, tempCookieList.getValue(item));
            }

            // Get the message and function according to the status
            // If the status is ok, show normal prompt
            // Else show error prompt
            
            if ("OK".equals(status)) {
                preValidMessage = ((G8RResponse) responseMessage).getMessage(); 
                System.out.print(preValidMessage);
            } else {
                System.err.print(((G8RResponse) responseMessage).getMessage());
            }

            // Get the function part, if function is NULL close all the resources and terminate 
            function = ((G8RResponse) responseMessage).getFunction();
            if ("NULL".equals(function)) {
                break;
            }
         // send the function with params
            sendInputParams(preValidMessage);

        }
    }

    /**
     * Close socket and files, then terminate the program
     * @throws IOException When I/O violation happens
     */
    public void close() throws IOException {
        cookieList.encode(outFile); // write the cookieList into outFile
        socket.close(); // Close the socket and its streams
        fis.close(); // close input file stream
        fos.close(); // close output file stream
        System.exit(0); // terminate the program
    }
    
    /**
     * Print out error message and terminate
     * 
     */
    private void printError(String message) {
        System.err.println(message);
        System.exit(0);
    }
    
    /**
     * Send messages with only user input function
     * When bad input happens, show prompt again and let the user input again
     * @throws IOException When I/O violation happens
     * 
     */
    private void sendInputFunction() throws IOException {
        while (true) {
            // Get the function part from console input
            System.out.print("Function>");
            function = sc.nextLine();
            // Send the initial request message to server
            try {
                (new G8RRequest(function, new String[0], cookieList)).encode(out);
                break; // if no violation happens, return to the main loop
            } catch (ValidationException e) { // if violation happens, read function again
                System.err.println("Bad user input: Function not a proper token (alphanumeric)");
            }
        }
    }
    
    /**
     * Send messages with only function and user input params
     * When bad input happens, show prompt again and let the user input again
     * @throws IOException When I/O violation happens
     * 
     */
    private void sendInputParams(String validMessage) throws IOException {
        while (true) {
            // Get the params part from console input
            String paramStr = sc.nextLine();
            params = paramStr.split(" ");
            // Send the request message to server
            try {
                // encode with params
                (new G8RRequest(function, params, cookieList)).encode(out);
                break; // if no violation happens, return to the main loop
            } catch (ValidationException e) { // if violation happens, read params again
                System.err.println("Bad user input: Param not a proper token (alphanumeric)");
                System.out.print(validMessage); // show the previous valid message to be the prompt
            }
        }
    }
}
