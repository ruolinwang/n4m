/**
 * File Name  :     G8RClient
 * Version    :     1.0
 * Author     :     Ruolin Wang
 * Date       :     02/12/2018
 * Function   :     This class provides the main class for G8RClient
 *                   
 * CopyRight Reserved
 * 
 */
package G8R.app;


import java.io.IOException;

import G8R.app.G8RSocketClient;

/**
 * Class Name: G8RClient
 * @author Ruolin Wang
 *
 */
public class G8RClient {
	
	private static G8RSocketClient socketClient = null; // reference to G8RSocketClient object

	/**
	 * Main function
	 * @param args Parameter(s): <Server> <Port> <Cookie File>
	 * 
	 */
	public static void main(String[] args)  {

		if (args.length != 3) { // Test for correct # of args here we throw exception(or system.err)
			throw new IllegalArgumentException("Parameter(s): <Server> <Port> <Cookie File>");
		}
		String server = args[0]; // Server name or IP address
        
		int servPort = 0;
		try {
		    servPort = Integer.parseInt(args[1]); // Server port for specified service  
		} catch (Exception e) {
		    System.err.println("The port num is invalid "+e.getMessage()); // if the port num has error show messages
		    System.exit(0);
		}
		

		String cookiesFileName = args[2]; // Cookie file name
		try {
			socketClient = new G8RSocketClient(server, servPort, cookiesFileName); //construct a G8RSocketClient
		} catch (Exception e) { // if construction fails show message and terminate
			System.err.println("Error happend when initializing: " + e.getMessage());
			System.exit(0);
		}
		
		try {
			socketClient.runClient(); // run the G8RSocketClient
		} catch (Exception e) {
			System.err.println("Error happend when communicating with server: " + e.getMessage());	// if error happens, show message		
		}
		finally {
		    try{
		        socketClient.close(); // close G8RSocketClient
		    }
		    catch (IOException e) {
		        System.err.println("Error happend when closing the client: " + e.getMessage()); // if close fails, show message
		    }
		    finally {
		        System.exit(0); // terminate
		    }		    
		}

	}
    
	
}
