/**
 * File Name  :     G8RServer
 * Version    :     2.0
 * Author     :     Ruolin Wang
 * Date       :     02/26/2018
 * Function   :     This class provides the main class for G8RServer 
 * Modified   :     Add the N4M function                  
 * CopyRight Reserved
 * 
 */

package G8R.app;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import N4M.app.N4MServer;

/**
 * Class Name: G8RServer
 * @author Ruolin Wang
 *
 */
public class G8RServer {

    private static final int MAXDPLENGTH = 65535;// Maximum datagramPacket length

    private static ServerSocket servSock = null;
    private static DatagramSocket udpSocket = null;
    private static Map<String, Integer> applications = new HashMap<String, Integer>();
    private static Lock lock = new ReentrantLock();
    private static Map<String, Long> timestamps = new HashMap<String, Long>();

    /**
     * Main function
     * @param args Parameter(s): <Port> <Threads>
     * 
     */
    public static void main(String[] args) {

        if (args.length != 2) { // Test for correct # of args
            throw new IllegalArgumentException("Parameter(s): <Port> <Threads>");
        }

        int ServPort = 0;
        try {
            ServPort = Integer.parseInt(args[0]); // Server port
        } catch (NumberFormatException e) {
            System.err.println("The server port is not integer " + e.getMessage()); // throw error information when exception happened
            System.exit(0); //terminate            
        }
        int threadPoolSize = 0;
        try {
            threadPoolSize = Integer.parseInt(args[1]); // Thread pool size
        } catch (NumberFormatException e) {
            System.err.println("The thread pool size is not integer " + e.getMessage()); // throw error information when exception happened
            System.exit(0); //terminate            
        }

        // create server socket
        try {
            servSock = new ServerSocket();
            servSock.setReuseAddress(true); // set reuseAddress so that we can restart at once
            servSock.bind(new InetSocketAddress(ServPort)); // set the port number

            udpSocket = new DatagramSocket(ServPort);

        } catch (Exception e) {
            System.err.println("Creating server socket fails, due to " + e.getMessage()); // throw error information when exception happened
            System.exit(0); //terminate
        }

        // create log file and build the logger
        final Logger logger = Logger.getLogger(G8RServer.class.getName());
        final Logger N4Mlogger = Logger.getLogger(N4MServer.class.getName());
        try {
            //create file handler pointing to the connections.log with append mode
            FileHandler fileHandler = new FileHandler("connections.log", true);
            fileHandler.setFormatter(new G8RLogFormatter());
            logger.addHandler(fileHandler);

            FileHandler N4MfileHandler = new FileHandler("n4m.log", true);
            N4MfileHandler.setFormatter(new G8RLogFormatter());
            N4Mlogger.addHandler(N4MfileHandler);
        } catch (Exception e) {
            System.err.println("Creating log file fails"); // throw error information when exception happened
            System.exit(0); //terminate
        }

        // Spawn a fixed number of threads to G8R service clients
        for (int i = 0; i < threadPoolSize; i++) {
            Thread thread = new Thread() {
                public void run() {
                    while (true) {
                        try {
                            Socket clntSock = servSock.accept(); // Wait for a connection
                            clntSock.setSoTimeout(30000); // set the time out to 30s for each client 
                            G8RProtocol process = new G8RProtocol(null,clntSock, logger, applications, timestamps, lock); // Create a process to implement poll protocol
                            process.handleG8RClient(); // Handle it
                        } catch (Exception ex) {
                            logger.log(Level.WARNING, "Error happened when communicating with client, thread id is "
                                    + Thread.currentThread().getId(), ex); // connection fails
                        }
                    }
                }
            };
            try {
                thread.start();
            } catch (Exception ex) {
                logger.log(Level.WARNING, "Thread start failed, thread no. is " + i, ex); // thread start fails
            }
        }

        // Spawn a fixed number of threads to N4M service clients
        for (int i = 0; i < threadPoolSize; i++) {
            Thread udpThread = new Thread() {
                public void run() {
                    while (true) {
                        try {
                            // Receive packet from client
                            DatagramPacket receivePacket = new DatagramPacket(new byte[MAXDPLENGTH], MAXDPLENGTH);
                            udpSocket.receive(receivePacket);
                            // handle the received packet
                            N4MServer N4MServerInst = new N4MServer(N4Mlogger, receivePacket, udpSocket,
                                    getNewTimeStamp(timestamps), applications);
                            N4MServerInst.handleProcess();
                        } catch (Exception ex) {
                            N4Mlogger.log(Level.WARNING,
                                    "Error happened when communicating with client, udpThread id is "
                                            + Thread.currentThread().getId(),
                                    ex); // connection fails
                        }
                    }
                }
            };
            try {
                udpThread.start();
            } catch (Exception ex) {
                N4Mlogger.log(Level.WARNING, "udpThread start failed, thread no. is " + i, ex); // thread start fails
            }
        }
    }

    private static long getNewTimeStamp(Map<String, Long> timestamps) {
        long result = 0L;
        for (String key : timestamps.keySet()) {
            if (result < timestamps.get(key)) {
                result = timestamps.get(key);
            }
        }
        return result;
    }

}
