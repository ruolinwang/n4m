/**
 * File Name  :     StatePoll
 * Version    :     1.0
 * Author     :     Ruolin Wang
 * Date       :     02/26/2018
 * Function   :     This class provides the implementation for BetStep State
 *                   
 * CopyRight Reserved
 * 
 */
package G8R.app.states.poll;

import java.io.IOException;

import G8R.app.G8RProtocol;
import G8R.app.states.State;

/**
 * Class Name  :     StatePoll
 * @author Ruolin Wang
 *
 */
public class StatePoll implements State {

    @Override
    public void process(G8RProtocol protocol) throws IOException {
        if (protocol.getRequestMessage().getCookieList().getNames().contains("LName")
                && protocol.getRequestMessage().getCookieList().getNames().contains("FName")) { // if the cookielist already contains LNAME and FNAME
            protocol.sendResponse("OK", "FoodStep",
                    protocol.getRequestMessage().getCookieList().getValue("FName") + "'s food mood>",
                    protocol.getRequestMessage().getCookieList()); // send message to client to input food mood
            protocol.logResponseMessage();
            protocol.setState(new StateFoodStep()); // change state to StateFoodStep
        } else {// if the cookielist doesn't contain LNAME and FNAME
            protocol.sendResponse("OK", "NameStep", "Name (First Last)>", protocol.getRequestMessage().getCookieList());// send message to inform client to write name
            protocol.logResponseMessage();
            protocol.setState(new StateNameStep()); // change state to StateNameStep
        }

    }

}
