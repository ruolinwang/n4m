/**
 * File Name  :     StateFoodStep
 * Version    :     1.0
 * Author     :     Ruolin Wang
 * Date       :     02/26/2018
 * Function   :     This class provides the implementation for BetStep State
 *                   
 * CopyRight Reserved
 * 
 */
package G8R.app.states.poll;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import G8R.app.G8RProtocol;
import G8R.app.states.State;
import G8R.serialization.CookieList;
import G8R.serialization.ValidationException;

/**
 *  Class Name  :     StateFoodStep
 * @author Ruolin Wang
 *
 */
public class StateFoodStep implements State {
    /* The hashmap to store choices and their values */
    private static final Map<String, String[]> foodRestaurantMap;
    static {
        foodRestaurantMap = new HashMap<String, String[]>();
        foodRestaurantMap.put("Mexican", new String[] { "20", "Tacopia" });
        foodRestaurantMap.put("Italian", new String[] { "25", "Pastastic" });
    }

    @Override
    public void process(G8RProtocol protocol) throws IOException, ValidationException {
        if (protocol.getRequestMessage().getParams().length != 1) { // invalid input and tell client input again
            protocol.sendResponse("ERROR", "FoodStep", "Poorly formed food mood. "
                    + protocol.getRequestMessage().getCookieList().getValue("FName") + "\'s food mood>");
            protocol.logResponseMessage();
        } else { // get the repeatTime and added by 1
            String repeatTime;
            CookieList cookieList = protocol.getRequestMessage().getCookieList();
            if ((repeatTime = cookieList.getValue("Repeat")) != null) { // cookielist get repeat
                try {
                    cookieList.add("Repeat", Integer.toString(Integer.parseInt(repeatTime) + 1));
                } catch (NumberFormatException e) { // if repeat is not integer
                    cookieList.add("Repeat", Integer.toString(1));
                }
            } else { // if no repeat
                cookieList.add("Repeat", Integer.toString(1));
            }
            /* get the choices and show informations about restaurant */
            String[] restaurant = foodRestaurantMap.get(protocol.getRequestMessage().getParams()[0]);
            protocol.sendResponse("OK", "NULL",
                    ((restaurant) == null ? "10" : restaurant[0]) + "% + " + cookieList.getValue("Repeat") + "% off at "
                            + ((restaurant) == null ? "McDonalds" : restaurant[1]),
                    cookieList);
            protocol.logResponseMessage();

        }

    }

}
