/**
 * File Name  :     StateNameStep
 * Version    :     1.0
 * Author     :     Ruolin Wang
 * Date       :     02/26/2018
 * Function   :     This class provides the implementation for BetStep State
 *                   
 * CopyRight Reserved
 * 
 */
package G8R.app.states.poll;

import java.io.IOException;

import G8R.app.G8RProtocol;
import G8R.app.states.State;
import G8R.serialization.CookieList;
import G8R.serialization.ValidationException;

/**
 * 
 * Class Name  :     StateNameStep
 * @author Ruolin Wang
 *
 */
public class StateNameStep implements State {

    @Override
    public void process(G8RProtocol protocol) throws IOException, ValidationException {
        if (protocol.getRequestMessage().getParams().length != 2) { // if invalid name
            protocol.sendResponse("ERROR", "NameStep", "Poorly formed name. Name (First Last)>",
                    protocol.getRequestMessage().getCookieList()); // send message to tell client rewrite names
            protocol.logResponseMessage();
        } else {
            CookieList cookieList = protocol.getRequestMessage().getCookieList();

            cookieList.add("LName", protocol.getRequestMessage().getParams()[1]); // add LName to cookielist
            cookieList.add("FName", protocol.getRequestMessage().getParams()[0]); // add FName to cookielist

            protocol.sendResponse("OK", "FoodStep", cookieList.getValue("FName") + "\'s food mood>", cookieList); // send message to client to input food mood
            protocol.logResponseMessage();
            protocol.setState(new StateFoodStep()); // change state
        }

    }

}
