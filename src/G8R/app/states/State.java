/**
 * File Name  :     State
 * Version    :     1.0
 * Author     :     Ruolin Wang
 * Date       :     02/26/2018
 * Function   :     This class provides the interface of all states
 *                   
 * CopyRight Reserved
 * 
 */


package G8R.app.states;

import java.io.IOException;

import G8R.app.G8RProtocol;
import G8R.serialization.ValidationException;
/**
 * Class Name  :     State
 * @author Ruolin Wang
 *
 */
public interface State
{
    /**
     * Method called by the Protocol
     * @param protocol the context of the state
     * @throws IOException 
     * @throws ValidationException 
     */
    public void process(G8RProtocol protocol) throws IOException, ValidationException;
    
}