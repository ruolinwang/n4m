/**
 * File Name  :     StateBetStep
 * Version    :     1.0
 * Author     :     Ruolin Wang
 * Date       :     02/26/2018
 * Function   :     This class provides the implementation for BetStep State
 *                   
 * CopyRight Reserved
 * 
 */
package G8R.app.states.game;

import java.io.IOException;

import G8R.app.G8RProtocol;
import G8R.app.states.State;
import G8R.serialization.CookieList;
import G8R.serialization.ValidationException;

/**
 * Class Name  :     StateBetStep
 * @author Ruolin Wang
 *
 */
public class StateBetStep implements State {

    @Override
    public void process(G8RProtocol protocol) throws IOException, ValidationException {
        CookieList cookieList = protocol.getRequestMessage().getCookieList();
        if (protocol.getRequestMessage().getParams().length != 1) { // check if the bet is only one integer
            protocol.sendResponse("ERROR", "BetStep", "Invalid Bet. Bet Again(5-20)>",
                    protocol.getRequestMessage().getCookieList()); // send error message
            protocol.logResponseMessage(); // log the message
        } else {
            try {
                int bet = Integer.parseInt(protocol.getRequestMessage().getParams()[0]);
                if (bet >= 5 && bet <= 20) { // right bet 
                    cookieList.add("Bet", protocol.getRequestMessage().getParams()[0]); // add to cookielist
                    protocol.sendResponse("OK", "GameStep", "Your choice(rock,paper,scissors)>", cookieList); // send response message
                    protocol.logResponseMessage(); // log response message
                    protocol.setState(new StateGameStep()); // change the state
                } else {
                    protocol.sendResponse("ERROR", "BetStep", "Invalid Bet. Bet Again(5-20)>", cookieList);// send error message
                    protocol.logResponseMessage();// log the message
                }

            } catch (NumberFormatException e) {
                protocol.sendResponse("ERROR", "BetStep", "Invalid Bet. Bet Again(5-20)>", cookieList);// send error message
                protocol.logResponseMessage();// log the message
            }
        }
    }

}
