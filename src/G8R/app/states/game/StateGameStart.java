/**
 * File Name  :     StateGameStart
 * Version    :     1.0
 * Author     :     Ruolin Wang
 * Date       :     02/26/2018
 * Function   :     This class provides the implementation for GameStart State
 *                   
 * CopyRight Reserved
 * 
 */
package G8R.app.states.game;

import java.io.IOException;

import G8R.app.G8RProtocol;
import G8R.app.states.State;
import G8R.serialization.CookieList;
import G8R.serialization.ValidationException;

/**
 * Class Name  :     StateGameStart
 * @author Ruolin Wang
 *
 */
public class StateGameStart implements State {
    private static final int DEFAULTCOINS = 1000; // the default total coins

    @Override
    public void process(G8RProtocol protocol) throws IOException, ValidationException {
        CookieList cookieList = protocol.getRequestMessage().getCookieList();
        String chargeMessage = ""; // the recharge message
        int totalCoin; // the total coins
        if (cookieList.getValue("TotalCoin") == null) { // if the cookielist contains no total coin information just create it by default values 
            totalCoin = DEFAULTCOINS;
            cookieList.add("TotalCoin", String.valueOf(DEFAULTCOINS)); // add to cookielist
        } else {
            try {
                totalCoin = Integer.parseInt(cookieList.getValue("TotalCoin")); // get the total coins form cookielist
                if (totalCoin < 20) { // if you coins are too less                
                    chargeMessage = "Only " + totalCoin + " coins left, auto recharged!"; // show the recharge message
                    totalCoin = DEFAULTCOINS; // recharge to the default coins
                    cookieList.add("TotalCoin", String.valueOf(DEFAULTCOINS)); // add to the cookielist 
                }
            } catch (NumberFormatException e) { // if the cookielist contains wrong format of coins
                totalCoin = DEFAULTCOINS; //recharge it
                cookieList.add("TotalCoin", String.valueOf(DEFAULTCOINS));// add to the cookielist
            }
        }
        protocol.sendResponse("OK", "BetStep", chargeMessage + "Total Coins: " + totalCoin + ". Your Bet(5-20)>",
                cookieList);// send response message
        protocol.logResponseMessage(); // log the message
        protocol.setState(new StateBetStep()); // change state to StateBetStep

    }
}
