/**
 * File Name  :     StateGameStep
 * Version    :     1.0
 * Author     :     Ruolin Wang
 * Date       :     02/26/2018
 * Function   :     This class provides the implementation for GameStep State
 *                   
 * CopyRight Reserved
 * 
 */
package G8R.app.states.game;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import G8R.app.G8RProtocol;
import G8R.app.states.State;
import G8R.serialization.CookieList;
import G8R.serialization.ValidationException;

/**
 * Class Name  :     StateGameStep
 * @author Ruolin Wang
 *
 */
public class StateGameStep implements State {
    /* The hashmap to store choices and their values */
    private static final Map<String, Integer> choiceMap;
    static {
        choiceMap = new HashMap<String, Integer>();
        choiceMap.put("rock", 1);
        choiceMap.put("paper", 2);
        choiceMap.put("scissors", 3);
    }

    @Override
    public void process(G8RProtocol protocol) throws IOException, ValidationException {
        CookieList cookieList = protocol.getRequestMessage().getCookieList();
        int totalCoin = 0;
        int bet = 0;
        try {
            totalCoin = Integer.parseInt(cookieList.getValue("TotalCoin")); // get total coins
            bet = Integer.parseInt(cookieList.getValue("Bet")); // get the bet
        } catch (NumberFormatException e) {

        }
        if (protocol.getRequestMessage().getParams().length != 1) { // no choice
            protocol.sendResponse("ERROR", "GameStep", "Invalid Choice. Your choice(rock,paper,scissors)>",
                    protocol.getRequestMessage().getCookieList());
            protocol.logResponseMessage();
        } else if (!"rock".equals(protocol.getRequestMessage().getParams()[0])
                && !"paper".equals(protocol.getRequestMessage().getParams()[0])
                && !"scissors".equals(protocol.getRequestMessage().getParams()[0])) { // invalid choice
            protocol.sendResponse("ERROR", "GameStep", "Invalid Choice. Your choice(rock,paper,scissors)>",
                    protocol.getRequestMessage().getCookieList());
            protocol.logResponseMessage();
        } else {
            Random rd = new Random(); // create random value to compare with client
            int myChoice = rd.nextInt(3) + 1;
            int yourChoice = choiceMap.get(protocol.getRequestMessage().getParams()[0]); // get the value of client choice
            if (myChoice == yourChoice) { // tie game
                protocol.sendResponse("OK", "GameStep", "Tie Game. Your choice(rock,paper,scissors)>",
                        protocol.getRequestMessage().getCookieList());
                protocol.logResponseMessage();
            } else if (!(myChoice == 1 && yourChoice == 3) && (yourChoice > myChoice)) {// client win
                cookieList.add("TotalCoin", String.valueOf(totalCoin + bet));
                protocol.sendResponse("OK", "NULL", "You Win! Total Coins: " + (totalCoin + bet), cookieList); // add the total coin with bet
                protocol.logResponseMessage();
            } else { // server win
                cookieList.add("TotalCoin", String.valueOf(totalCoin - bet));
                protocol.sendResponse("OK", "NULL", "You Lose! Total Coins: " + (totalCoin - bet), cookieList);// deduct the total coin with bet
                protocol.logResponseMessage();
            }
        }
    }

}
