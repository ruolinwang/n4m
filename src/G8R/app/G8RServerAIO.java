/**
 * File Name  :     G8RServerAIO
 * Version    :     2.0
 * Author     :     Ruolin Wang
 * Date       :     02/26/2018
 * Function   :     This class provides the main class for G8RServer 
 * Modified   :     Add the N4M function                  
 * CopyRight Reserved
 * 
 */

package G8R.app;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import N4M.app.N4MServer;

/**
 * Class Name: G8RServerAIO
 * @author Ruolin Wang
 *
 */
public class G8RServerAIO {
    /**
     * Buffer size (bytes)
     */
    private static final int BUFSIZE = 256;
    private static final int MAXDPLENGTH = 65535;// Maximum datagramPacket length

    private static DatagramSocket udpSocket = null;
    private static Map<String, Integer> applications = new HashMap<String, Integer>();
    private static Lock lock = new ReentrantLock();
    private static Map<String, Long> timestamps = new HashMap<String, Long>();

    /**
     * Global logger
     */
    private static final Logger logger = Logger.getLogger("G8RServerAIO");

    /**
     * 
     * The main function for G8RServerAIO
     * @param args Parameter(s): <Port>
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {

        if (args.length != 1) { // Test for correct # of args
            throw new IllegalArgumentException("Parameter(s): <Port>");
        }

        // create log file and build the logger

        final Logger N4Mlogger = Logger.getLogger(N4MServer.class.getName());
        try {
            //create file handler pointing to the connections.log with append mode
            FileHandler fileHandler = new FileHandler("connections.log", true);
            fileHandler.setFormatter(new G8RLogFormatter());
            logger.addHandler(fileHandler);

            FileHandler N4MfileHandler = new FileHandler("n4m.log", true);
            N4MfileHandler.setFormatter(new G8RLogFormatter());
            N4Mlogger.addHandler(N4MfileHandler);
        } catch (Exception e) {
            System.err.println("Creating log file fails"); // throw error information when exception happened
            System.exit(0); //terminate
        }

        // create N4M server socket
        try {

            udpSocket = new DatagramSocket(Integer.parseInt(args[0]));

        } catch (Exception e) {
            System.err.println("Creating N4M server socket fails, due to " + e.getMessage()); // throw error information when exception happened
            System.exit(0); //terminate
        }

        // Spawn a fixed number of threads to N4M service clients
        for (int i = 0; i < 1; i++) {
            Thread udpThread = new Thread() {
                public void run() {
                    while (true) {
                        try {
                            // Receive packet from client
                            DatagramPacket receivePacket = new DatagramPacket(new byte[MAXDPLENGTH], MAXDPLENGTH);
                            udpSocket.receive(receivePacket);
                            // handle the received packet
                            N4MServer N4MServerInst = new N4MServer(N4Mlogger, receivePacket, udpSocket,
                                    getNewTimeStamp(timestamps), applications);
                            N4MServerInst.handleProcess();
                        } catch (Exception ex) {
                            N4Mlogger.log(Level.WARNING,
                                    "Error happened when communicating with client, udpThread id is "
                                            + Thread.currentThread().getId(),
                                    ex); // connection fails
                        }
                    }
                }
            };
            try {
                udpThread.start();
            } catch (Exception ex) {
                N4Mlogger.log(Level.WARNING, "udpThread start failed, thread no. is " + i, ex); // thread start fails
            }
        }
        
        
        
        // create the AsynchronousServerSocketChannel
        try (AsynchronousServerSocketChannel listenChannel = AsynchronousServerSocketChannel.open()) {
            // Bind local port
            listenChannel.bind(new InetSocketAddress(Integer.parseInt(args[0])));

            // Create accept handler
            listenChannel.accept(null, new CompletionHandler<AsynchronousSocketChannel, Void>() {

                @Override
                public void completed(AsynchronousSocketChannel clntChan, Void attachment) {
                    listenChannel.accept(null, this);
                    try {
                        handleAccept(clntChan);
                    } catch (IOException e) {
                        failed(e, null);
                    }
                }

                @Override
                public void failed(Throwable e, Void attachment) {
                    logger.log(Level.WARNING, "Close Failed", e);
                }
            });

            // Block until current thread dies
            Thread.currentThread().join();
        } catch (InterruptedException e) {
            logger.log(Level.WARNING, "Server Interrupted", e);
        }

    }

    /**
     * Called after each accept completion
     * 
     * @param clntChan channel of new client
     * @throws IOException if I/O problem
     */
    public static void handleAccept(final AsynchronousSocketChannel clntChan) throws IOException {
        ByteBuffer buf = ByteBuffer.allocateDirect(BUFSIZE);
        clntChan.read(buf, buf, new CompletionHandler<Integer, ByteBuffer>() {
            public void completed(Integer bytesRead, ByteBuffer buf) {
                try {
                    ByteArrayOutputStream messageBuffer = new ByteArrayOutputStream();
                    G8RProtocol protocol = new G8RProtocol(clntChan, null, logger, applications, timestamps, lock);
                    protocol.handleRead(clntChan, buf, bytesRead, messageBuffer);
                } catch (Exception e) {
                    logger.log(Level.WARNING, "Handle Read Failed", e);
                }
            }

            public void failed(Throwable ex, ByteBuffer v) {
                try {
                    clntChan.close();
                } catch (IOException e) {
                    logger.log(Level.WARNING, "Close Failed", e);
                }
            }
        });
    }

    private static long getNewTimeStamp(Map<String, Long> timestamps) {
        long result = 0L;
        for (String key : timestamps.keySet()) {
            if (result < timestamps.get(key)) {
                result = timestamps.get(key);
            }
        }
        return result;
    }

}
