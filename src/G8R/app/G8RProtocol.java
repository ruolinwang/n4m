/**
 * File Name  :     G8RProtocol
 * Version    :     1.0
 * Author     :     Ruolin Wang
 * Date       :     02/26/2018
 * Function   :     This class provides the basic method for each thread using state design pattern
 *                   
 * CopyRight Reserved
 * 
 */
package G8R.app;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.logging.Level;

import java.util.logging.Logger;

import G8R.app.states.State;
import G8R.app.states.game.StateGameStart;
import G8R.app.states.poll.StatePoll;
import G8R.serialization.CookieList;
import G8R.serialization.G8RMessage;
import G8R.serialization.G8RRequest;
import G8R.serialization.G8RResponse;
import G8R.serialization.MessageInput;
import G8R.serialization.MessageOutput;
import G8R.serialization.ValidationException;

/**
 * Class Name  :     G8RProtocol
 * @author Ruolin Wang
 *
 */
public class G8RProtocol {

    private AsynchronousSocketChannel clntChan = null; // Socket connect to client
    private static final String DELIMITER = "\r\n\r\n";
    private Socket clntSock = null; // Socket connect to client
    private Logger logger = null; // Server logger
    private State state = null; // to store the state
    private MessageInput in = null; // to buffer the inputStream
    private MessageOutput out = null; // to buffer the outputStream
    private G8RMessage requestMessage = null; // the request message received from client
    private G8RResponse responseMessage = null; // the response message sent back to client
    private Map<String, Integer> applications = null; // to store the number of application used
    private Lock lock = null;
    private Map<String, Long> timestamps = null;

    /**
     * Creates a new G8RProtocol using given values
     * @param clntChan the AsynchronousServerSocketChannel
     * @param clntSock the client socket 
     * @param logger the server logger
     * @param applications 
     * @param timestamps the app name and timestamps pair
     * @param lock 
     */
    public G8RProtocol(AsynchronousSocketChannel clntChan, Socket clntSock, Logger logger,
            Map<String, Integer> applications, Map<String, Long> timestamps, Lock lock) {
        this.clntChan = clntChan;
        this.clntSock = clntSock;
        this.logger = logger;
        this.applications = applications;
        this.lock = lock;
        this.timestamps = timestamps;
    }

    /**
     * 
     * Receive the message from client and run different function based on the different state
     * @param clntChan the AsynchronousServerSocketChannel
     * @param buf the buffer to store data
     * @param bytesRead the read byte number
     * @param messageBuffer the buffer to store the message
     * @throws IOException when communicating with client fails
     * @throws ValidationException when message is invalid
     *  
     */

    public void handleRead(final AsynchronousSocketChannel clntChan, ByteBuffer buf, int bytesRead,
            final ByteArrayOutputStream messageBuffer) throws IOException, ValidationException {
        if (bytesRead == -1) { // Did the other end close?
            clntChan.close();
        } else if (bytesRead > 0) {
            buf.flip(); // prepare to write

            String receiveBuf = StandardCharsets.UTF_8.decode(buf).toString();
            String tempMessage = messageBuffer.toString() + receiveBuf; // get the whole message 

            if (tempMessage.indexOf(DELIMITER) > 0) {// if get the delimiter
                messageBuffer.close();
                handleRequest(tempMessage.substring(0, tempMessage.indexOf(DELIMITER) + DELIMITER.length())); // go to handle request
            } else { // otherwise read again
                messageBuffer.write(receiveBuf.getBytes());
                buf.clear();
                clntChan.read(buf, buf, new CompletionHandler<Integer, ByteBuffer>() {
                    public void completed(Integer bytesRead, ByteBuffer buf) {
                        try {
                            handleRead(clntChan, buf, bytesRead, messageBuffer);
                        } catch (Exception e) {
                            logger.log(Level.WARNING, "Handle Read Failed", e);
                        }
                    }

                    public void failed(Throwable ex, ByteBuffer v) {
                        try {
                            clntChan.close();
                        } catch (IOException e) {
                            logger.log(Level.WARNING, "Close Failed", e);
                        }
                    }
                });
            }

        }
    }

    /**
     * Called after each write
     * 
     * @param clntChan channel of new client
     * @param buf byte buffer used in write
     * @throws IOException if I/O problem
     */
    public void handleWrite(final AsynchronousSocketChannel clntChan, ByteBuffer buf, Boolean isWrongCommand)
            throws IOException {
        if (buf.hasRemaining()) { // More to write
            clntChan.write(buf, buf, new CompletionHandler<Integer, ByteBuffer>() {
                public void completed(Integer bytesWritten, ByteBuffer buf) {
                    try {
                        handleWrite(clntChan, buf, isWrongCommand);
                    } catch (IOException e) {
                        logger.log(Level.WARNING, "Handle Write Failed", e);
                    }
                }

                public void failed(Throwable ex, ByteBuffer buf) {
                    try {
                        clntChan.close();
                    } catch (IOException e) {
                        logger.log(Level.WARNING, "Close Failed", e);
                    }
                }
            });
        } else { // Back to reading
            if (isWrongCommand) {
                return;
            }
            buf.clear();
            clntChan.read(buf, buf, new CompletionHandler<Integer, ByteBuffer>() {
                public void completed(Integer bytesRead, ByteBuffer buf) {
                    try {
                        ByteArrayOutputStream messageBuffer = new ByteArrayOutputStream();
                        handleRead(clntChan, buf, bytesRead, messageBuffer);
                    } catch (Exception e) {
                        logger.log(Level.WARNING, "Handle Read Failed in handle write", e);
                    }
                }

                public void failed(Throwable ex, ByteBuffer v) {
                    try {
                        clntChan.close();
                    } catch (IOException e) {
                        logger.log(Level.WARNING, "Close Failed", e);
                    }
                }
            });
        }
    }

    /**
     * @param message the request message
     * @throws IOException if I/O problem
     * @throws ValidationException
     */
    public void handleRequest(String message) throws IOException, ValidationException {
        in = new MessageInput(new ByteArrayInputStream(message.getBytes()));
        ByteArrayOutputStream tempOut = new ByteArrayOutputStream();
        out = new MessageOutput(tempOut);
        // Decode the message received from the server and log it
        setRequestMessage((G8RRequest) G8RMessage.decode(in));
        logRequestMessage();

        // get the function from client message and enter the different function state
        Boolean isWrongCommand = false;
        if (state == null) {
            switch (requestMessage.getFunction()) {
            case "Poll": { // the same function as Dr. Donahoo
                setApplications(requestMessage.getFunction());
                setState(new StatePoll());// enter the poll function first state
                isWrongCommand = false;
                break;
            }
            case "Game": { // my own function
                setApplications(requestMessage.getFunction());
                setState(new StateGameStart()); // enter the game function first state
                isWrongCommand = false;
                break;
            }
            default: { // default case tell the client unexpected function
                sendResponse("ERROR", "NULL", "Unexpected Function"); // send error response message
                logResponseMessage(); // send error response message
                logError(); // log error and terminate
                isWrongCommand = true;
                break;
            }
            }
        }
        if (!isWrongCommand) {
            state.process(this);
        }
        ByteBuffer tempBuf = ByteBuffer.wrap(tempOut.toByteArray());
        handleWrite(clntChan, tempBuf, isWrongCommand);
        if(isWrongCommand) {
            clntChan.close(); // close the client socket
        }
    }

    /**
     * 
     * Receive the message from client and run different function based on the different state
     * @throws IOException when communicating with client fails
     *  
     */
    public void handleG8RClient() throws IOException {

        try {

            // Get the input and output I/O streams from socket
            // Encapsulate the input and output I/O streams inside MessageInput and
            // MessageOutput
            in = new MessageInput(clntSock.getInputStream());
            out = new MessageOutput(clntSock.getOutputStream());

            // Decode the message received from the server and log it
            setRequestMessage((G8RRequest) G8RMessage.decode(in));
            logRequestMessage();

            // get the function from client message and enter the different function state
            switch (requestMessage.getFunction()) {
            case "Poll": { // the same function as Dr. Donahoo
                setApplications(requestMessage.getFunction());
                setState(new StatePoll());// enter the poll function first state
                break;
            }
            case "Game": { // my own function
                setApplications(requestMessage.getFunction());
                setState(new StateGameStart()); // enter the game function first state
                break;
            }
            default: { // default case tell the client unexpected function
                sendResponse("ERROR", "NULL", "Unexpected Function"); // send error response message
                logResponseMessage(); // send error response message
                logError(); // log error and terminate
                clntSock.close(); // close the client socket
                return;
            }
            }

            while (true) {
                // Run different process method based on the state
                state.process(this);

                // Decode the message received from the server and log the message
                setRequestMessage((G8RRequest) G8RMessage.decode(in));
                logRequestMessage();
            }

        } catch (IOException ex) { // If IOException happened terminate the client connection
            if (requestMessage == null) { // If nothing read in we need to record the client info and close it
                logger.log(Level.INFO, System.lineSeparator() + clntSock.getInetAddress().toString() + ":"
                        + clntSock.getPort() + "-" + Thread.currentThread().getId());
            }
            logError();// log error and terminate
        } catch (ValidationException ex) {
            sendResponse("ERROR", "NULL", ex.getMessage());// send error response message
            logResponseMessage(); // send error response message
            logError();// log error and terminate
        } finally {
            try {
                clntSock.close(); // close client socket
            } catch (IOException e) {
                throw new IOException("Client socket close error."); // if close fails
            }
        }
    }

    /**
     * Set the state of this context
     * @param state new state of the context
     */
    public void setState(State state) // call by the state class
    {
        this.state = state;
    }

    /**
     * Call by each state so that they can get the message
     * @return request message from client
     */
    public G8RRequest getRequestMessage() {
        return (G8RRequest) requestMessage;
    }

    /**
     * Set the requestMessage 
     * @param requestMessage new request Message
     */
    public void setRequestMessage(G8RRequest requestMessage) {
        this.requestMessage = requestMessage;
    }

    /**
     * 
     * send the response message to the client
     * 
     * 
     * @param status response status
     * @param function response function
     * @param message response message
     * @param cookieList response cookieList
     * @throws IOException when sending fails
     */
    public void sendResponse(String status, String function, String message, CookieList... cookieList)
            throws IOException {
        try {
            if (cookieList.length == 0) { // if no cookielist input, create an empty CookieList
                (responseMessage = new G8RResponse(status, function, message, new CookieList())).encode(out);
            } else if (cookieList.length == 1) { // if there is an cookielist input, just use it
                (responseMessage = new G8RResponse(status, function, message, cookieList[0])).encode(out);
            }
        } catch (ValidationException e) {
            // only for debugging by server developer
            // after development, there should be no validation exception in this method
            System.err.println(e.getMessage());
        }

    }

    /**
     * Log the response message sent to client
     */
    public void logResponseMessage() {
        logger.log(Level.INFO, "Sent:" + responseMessage.toSimpleString());
    }

    /**
     * Log the error message
     */
    private void logError() {
        logger.log(Level.INFO, "***client terminated" + System.lineSeparator());
    }

    /**
     * Log the request message received from client
     * @throws IOException 
     */
    private void logRequestMessage() throws IOException {
        if (clntSock == null) {
            logger.log(Level.INFO,
                    System.lineSeparator() + clntChan.getRemoteAddress().toString() + "-"
                            + Thread.currentThread().getId() + " Received:"
                            + ((G8RRequest) requestMessage).toSimpleString() + "|");
        } else {
            logger.log(Level.INFO,
                    System.lineSeparator() + clntSock.getInetAddress().toString() + ":" + clntSock.getPort() + "-"
                            + Thread.currentThread().getId() + " Received:"
                            + ((G8RRequest) requestMessage).toSimpleString() + "|");
        }

    }

    private void setApplications(String name) {
        lock.lock();
        try {
            if (this.applications.containsKey(name)) {
                applications.put(name, applications.get(name) + 1);
            } else {
                applications.put(name, 1);
            }
            timestamps.put(name, new Date().getTime() / 1000); // set the newest timestamp
        } catch (Exception e) {
            System.err.println(e.getMessage());
        } finally {
            lock.unlock();
        }
    }

}
