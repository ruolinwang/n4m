/**
 * File Name  :     CookieList
 * Version    :     1.0
 * Author     :     Ruolin Wang
 * Date       :     01/17/2018
 * Function   :     This class provides basic methods for manipulating the cookies contained in one 
 *                  G8R messages
 *                   
 * CopyRight Reserved
 * 
 */
package G8R.serialization;

import java.io.EOFException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

/**
 * 
 * Class Name : CookieList
 * @author Ruolin Wang
 *
 */
public class CookieList {

	private TreeMap<String, String> cookies; // the data structure contains all the cookies which is the real cookielist

	private static final String DELIMITER = "\r\n"; // cookie delimiter

	/**
	 * Creates a new, empty cookie list
	 */
	public CookieList() {
		this.cookies = new TreeMap<String, String>(); // create empty cookieslist
	}

	/**
	 * Creates a new, cloned cookie list
	 * @param cookieList list of cookies to clone
	 * @exception java.lang.NullPointerException - if cookies is null
	 */
	public CookieList(CookieList cookieList) {
		Objects.requireNonNull(cookieList, "cookieList is null"); //judge if input cookieList is null
		this.cookies = new TreeMap<String, String>(cookieList.cookies); // create cookies by input cookieList

	}

	/**
	 * Creates a new CookieList by deserializing from the given input according to the specified serialization.
	 * @param in input stream from which to deserialize the name/value list
	 * @exception ValidationException if validation problem such as illegal name and/or value, etc.
	 * @exception java.io.IOException if I/O problem (EOFException for EoS)
	 * @exception java.lang.NullPointerException if input stream is null
	 */
	public CookieList(MessageInput in) throws ValidationException, IOException {
		Objects.requireNonNull(in, "MessageInput is null for CookieList(MessageInput in)");
		this.cookies = new TreeMap<String, String>();

		/*
		 * The main loop stop in two cases: 1. the cookielist read the end cookie a. at
		 * the beginning it is an end cookie b. read several valid cookies and read end
		 * cookie 2. the cookies in the cookielist has error and just stop by throwing
		 * exception
		 * 
		 */
		String cookie; // store a cookie
		while (!DELIMITER.equals(cookie = in.getToken(DELIMITER))) { // get the cookie and check it 

			// cookie is empty means we read an EOF before data
			if (cookie.isEmpty()) {
				throw new EOFException("read the EOF of inputstream with no cookie in the list");
			}

			// cookie is shorter than the delimiter length means we do read data but also read
			// an EOF before read a delimiter
			if (cookie.length() < DELIMITER.length()
					|| !cookie.substring(cookie.length() - DELIMITER.length()).equals(DELIMITER)) {
				throw new EOFException("Non-empty message without delimiter indicating an EOF");
			}

			// split the cookie (without delimiter) and get the token
			String cookieToken[] = cookie.substring(0, cookie.length() - DELIMITER.length()).split("=");

			// name and value in a cookie should be a pair, check this 
			if (cookieToken.length != 2 || (cookieToken[0].equals("") && !cookieToken[1].equals(""))) {
				throw new ValidationException("the number of value and name don't match", cookie);
			}

			// name is not alphanumeric
			if (!isValidToken(cookieToken[0])) {
				throw new ValidationException("cookie's" + " name is not alphanumeric.", cookie);
			}

			// value is not alphanumeric
			if (!isValidToken(cookieToken[1])) {
				throw new ValidationException("cookie's" + " value is not alphanumeric.", cookie);
			}

			// no error in a cookie so that store it into cookielist
			this.cookies.put(cookieToken[0], cookieToken[1]);
		}
	}

	/**
	 * Encode the name-value list. The name-value pair serialization must be in sort order (alphabetically by name in increasing order). For example, a=1 b=2 ...
	 * @param out serialization output sink
	 * @exception java.io.IOException if I/O problem
	 * @exception java.lang.NullPointerException if out is null
	 */
	public void encode(MessageOutput out) throws IOException {
		Objects.requireNonNull(out, "MessageOutput is null for CookieList(MessageInput in)");

		String key = null; // the key in a cookie
		String value = null; // the value in a cookie 
		Iterator<String> iter = cookies.keySet().iterator(); // traverse the cookies by iterator
		while (iter.hasNext()) { // still have tuples in cookies
			key = (String) iter.next(); // key
			value = (String) cookies.get(key); // value
			out.write(key + "=" + value + DELIMITER); // write out one cookies
			out.flush(); // this method can be called if you want to flush the data into real outputstream
		}
		out.write(DELIMITER); // write out the end delimiter of cookielist
		out.flush(); // this method can be called if you want to flush the data into real outputstream

	}

	/**
	 * Adds the name/value pair. If the name already exists, the new value replaces the old value
	 * @param name name to be added
	 * @param value value to be associated with the name
	 * @exception ValidationException if validation failure for name or value
	 * @exception java.lang.NullPointerException if name or value is null
	 */
	public void add(String name, String value) throws ValidationException {
		Objects.requireNonNull(name, "name is null"); // name is not null
		Objects.requireNonNull(value, "value is null"); // value is not null
		if (!isValidToken(name)) { // judge if the name is all alphanumeric
			throw new ValidationException("name is not alphanumeric", name);
		}
		if (!isValidToken(value)) { // judge if the value is all alphanumeric
			throw new ValidationException("value is not alphanumeric", value);
		}
		cookies.put(name, value);

	}

	/**
	 * Returns string representation of cookie list. 
	 * The name/value pair serialization must be in sort order (alphabetically by name in increasing order). 
	 * For example, Cookies=[a=1,b=2] You must follow the spacing, etc. precisely. 
	 * If there are no cookies, encode as Cookies=[] Note: Space (or lack thereof) is important
	 * @return string representation of cookie list
	 */
	@Override
	public String toString() {
		String result = ""; // to store the output
		String key = null; // the key in a cookie
		String value = null; // the value in a cookie 
		Iterator<String> iter = cookies.keySet().iterator(); // traverse the cookies by iterator
		while (iter.hasNext()) {
			key = (String) iter.next(); // key
			value = (String) cookies.get(key);// value
			result += (key + "=" + value + ","); // get one cookie
		}
		if (result.length() == 0) { // if there is no cookies which is valid, return Cookies=[]
			return "Cookies=[]";
		}
		result = "Cookies=[" + result.substring(0, result.length() - 1) + "]"; // remove the last "," from the string
		return result;
	}

	/**
	 * Gets the set of names
	 * @return Set (potentially empty) of names (strings) for this list
	 */
	public Set<String> getNames() {

		Set<String> keys = new HashSet<String>(cookies.keySet()); // give out a new hard copy
		return keys;
	}

	/**
	 * Gets the value associated with the given name
	 * @param name cookie name
	 * @return Value associated with the given name or null if no such name
	 */
	public String getValue(String name) {
		return cookies.get(name);
	}

	/**
	 * Implement according to Object.hashCode()
	 * 
	 */
	@Override
	public int hashCode() {
		return Objects.hash(cookies);
	}

	/**
	 * Implement according to Object.equals(java.lang.Object)
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if (obj == null || obj.getClass() != this.getClass()) {
			result = false;
		} else {
			CookieList cl = (CookieList) obj;
			if (this.cookies.equals(cl.cookies)) {
				result = true;
			}

		}
		return result;
	}

	/**
	 * check whether the name or value for a cookie is valid(a sequence (length>=1) of alphanumeric characters).
	 * @param token token is the name or value for a cookie
	 * @return if the input token is a sequence (length>=1) of alphanumeric characters, return true. Otherwise false.
	 */
	private boolean isValidToken(String token) {
		String regex = "^[A-Za-z0-9]+$";
		return token.matches(regex);
	}
}
