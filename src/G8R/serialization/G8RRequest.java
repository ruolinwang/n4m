/**
 * File Name  :     G8RRequest
 * Version    :     1.0
 * Author     :     Ruolin Wang
 * Date       :     01/17/2018
 * Function   :     This class provides basic methods for G8RRequest
 *                   
 * CopyRight Reserved
 * 
 */
package G8R.serialization;

import java.util.Arrays;
import java.util.Objects;
import java.util.Set;

/**
 * Class Name : G8RRequest
 * @author Ruolin Wang
 *
 */
public class G8RRequest extends G8RMessage {

    private static final String SPACEDELIMITER = " "; // the delimiter of space
    private static final String RETURNDELIMITER = "\r\n"; // the delimiter of return
    private static final String HEAD = "G8R/1.0 Q RUN "; // the valid head for request message

    private String[] params; // the params part in request message

    /**
     * Creates a new G8R request using given values
     * @param function the function part in request message
     * @param params the params part in request message
     * @param cookieList the cookieList part in request message
     * @throws ValidationException if validation fails
     * @throws NullPointerException - if null parameter
     */
    public G8RRequest(String function, String[] params, CookieList cookieList) throws ValidationException {

        setFunction(function);
        setParams(params);
        setCookieList(cookieList);
    }

    /**
     * Returns human-readable, complete (i.e., all attribute values), string representation of G8R request message
     * The form is exactly the same pattern in specification 
     * @return string representation of request message
     */
    @Override
    public String toString() {
        String message = "G8R/1.0 Q RUN " + function;
        for (String item : params) { // get all the params
            message += SPACEDELIMITER + item;
        }
        message += RETURNDELIMITER;
        Set<String> names = cookieList.getNames();
        for (String item : names) { // get all the name-value pairs in cookielist
            message += item + "=" + cookieList.getValue(item) + RETURNDELIMITER;
        }
        message += RETURNDELIMITER;
        return message;
    }

    /**
     * Returns human-readable, complete (i.e., all attribute values), string representation of G8R request message
     * The form is a simple connection of all parts 
     * @return string representation of request message
     */
    public String toSimpleString() {
        String message = "G8R/1.0 Q RUN " + function;
        for (String item : params) { // get all the params
            message += SPACEDELIMITER + item;
        }
        message += SPACEDELIMITER;
        message += cookieList.toString();
        return message;
    }

    /**
     * Returns params in request message. 
     * Return a hard copy of the params.
     * @return a string array contains all the params 
     */
    public String[] getParams() {
        String[] output = params.clone();
        return output;

    }

    /**
     * Set the params in request message. 
     * Check each item of the params and this is also a hard copy.
     * @param params input params
     * @throws ValidationException invalid params
     * @throws NullPointerException - if null array or array element
     * 
     */
    public void setParams(String[] params) throws ValidationException {
        Objects.requireNonNull(params, "params is null");
        for (String item : params) {
            Objects.requireNonNull(item, "param has null item"); // check if the param contains null item
            if (!isValidToken(item)) {
                throw new ValidationException("Invalid param", item);
            }
        }

        this.params = params.clone();
    }

    /**
     * Encode the entire G8R request message
     * @param out serialization output sink
     * @throws java.io.IOException - if I/O problem
     * @throws java.lang.NullPointerException - if out is null
     */
    @Override
    public void encode(MessageOutput out) throws java.io.IOException {
        out.write(HEAD + function); // encode the head and function
        for (String item : params) { // encode params
            out.write(SPACEDELIMITER + item);
        }
        out.write(RETURNDELIMITER);
        out.flush(); // flush all the data
        cookieList.encode(out); // encode cookielist
    }

    /**
     * Implement according to Object.hashCode()
     * 
     */
    @Override
    public int hashCode() {
        return function.hashCode() * 64 + Arrays.asList(this.params).hashCode() * 32 + cookieList.hashCode();
    }

    /**
     * Implement according to Object.equals(java.lang.Object)
     * 
     */
    @Override
    public boolean equals(Object obj) {
        boolean result = false;
        if (obj == null || obj.getClass() != this.getClass()) {
            result = false;
        } else {
            G8RRequest request = (G8RRequest) obj;
            if (this.cookieList.equals(request.cookieList) && this.function.equals(request.function)
                    && Arrays.asList(this.params).equals(Arrays.asList(request.params))) {
                result = true;
            }

        }
        return result;
    }
}
