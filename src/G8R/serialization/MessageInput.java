/**
 * File Name  :     MessageInput
 * Version    :     2.0
 * Author     :     Ruolin Wang
 * Date       :     01/17/2018
 * Function   :     This class can buffer the inputStream and parse the inputStream in the unit of cookie(the message can be very large)
 *                  Each time the top layer can get a cookie from the buffer pool by getNextEntry() method
 *                                   
 * CopyRight Reserved
 * 
 */
package G8R.serialization;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

/**
 * Class Name : MessageInput
 * @author Ruolin Wang
 *
 */
public class MessageInput {

	/* used for buffer the inputStream */
	private InputStreamReader inputStreamReader;

	/**
	 * Constructs a new input source from an InputStream
	 * 
	 * @param in byte input source            
	 * @exception NullPointerException if in is null
	 *                  
	 */
	public MessageInput(InputStream in) throws NullPointerException {
		Objects.requireNonNull(in, "Input is null for MessageInput(InputStream in)"); // a simple way to test if the input is null

		inputStreamReader = new InputStreamReader(in, StandardCharsets.US_ASCII); // use this form to avoid producing other exceptions

	}

	/**
	 * 
	 * Give out a token based on one input delimiter, the token contains delimiter
	 * @param delimiter the delimiter we use to separate tokens
	 * @return one token in string form such as "a=1\r\n"
	 * @throws IOException 
	 *                  
	 */

	public String getToken(String delimiter) throws IOException {

		int nextByte = 0; // used for read bytes from inputStreamReader
		String tokenBuffer = ""; // to store a token
		String delimiterSearcher = ""; // used for searching the token delimiter

		/*
		 * The main while loop stop when reading the pattern of delimiter
		 */
		while (!delimiter.equals(delimiterSearcher)) {
			nextByte = inputStreamReader.read();
			if (nextByte == -1) { // end of stream?
				break;
			}
			tokenBuffer += (char) nextByte; // write byte to buffer
			if (tokenBuffer.length() >= delimiter.length()) {
				delimiterSearcher = tokenBuffer.substring(tokenBuffer.length() - delimiter.length()); // create searcher to match the delimiter
			}
		}
		return tokenBuffer;
	}

	/**
	 * 
	 * Give out a token based on two input delimiters, the token contains delimiters
	 * The token will be picked up by any of the two delimiters(use the first met one)
	 * @param firstDelimiter the first delimiter we use to separate tokens
	 * @param secondDelimiter the second delimiter we use to separate tokens
	 * @return one token in string form such as "a=1\r\n"
	 * @throws IOException 
	 *                  
	 */
	public String getToken(String firstDelimiter, String secondDelimiter) throws IOException {

		int nextByte = 0; // used for read bytes from inputStreamReader
		String tokenBuffer = ""; // to store a token
		String firstSearcher = ""; // used for searching the first token delimiter
		String secondSearcher = ""; // used for searching the second token delimiter
		/*
		 * The main while loop stop when reading the pattern of firstDelimiter or
		 * secondDelimiter
		 */
		while (!firstDelimiter.equals(firstSearcher) && !secondDelimiter.equals(secondSearcher)) {
			nextByte = inputStreamReader.read();
			if (nextByte == -1) { // end of stream?
				break;
			}
			tokenBuffer += (char) nextByte; // write byte to buffer
			if (tokenBuffer.length() >= firstDelimiter.length()) { // create searcher to match the first delimiter
				firstSearcher = tokenBuffer.substring(tokenBuffer.length() - firstDelimiter.length());
			}
			if (tokenBuffer.length() >= secondDelimiter.length()) {// create searcher to match the second delimiter
				secondSearcher = tokenBuffer.substring(tokenBuffer.length() - secondDelimiter.length());
			}
		}
		return tokenBuffer;
	}
	
}
