/**
 * File Name  :     G8RMessage
 * Version    :     1.0
 * Author     :     Ruolin Wang
 * Date       :     01/17/2018
 * Function   :     This class provides basic decode and encode methods for general G8R Message 
 *                   
 * CopyRight Reserved
 * 
 */

package G8R.serialization;

import java.io.EOFException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

/**
 * 
 * Class Name : G8RMessage
 * @author Ruolin Wang
 *
 */
public class G8RMessage {

	private static final String SPACEDELIMITER = " "; // the delimiter of space
	private static final String RETURNDELIMITER = "\r\n"; // the delimiter of return
	private static final String HEAD = "G8R/1.0 "; // the header in specification
	private static final String REQUEST = "Q "; // Q stands for Request in message
	private static final String RESPONSE = "R "; // R stands for Response in message
	private static final String COMMAND = "RUN "; // the command in specification
	private static final String SPACETOKEN = "^[A-Za-z0-9]+\\s$"; // the valid token pattern with space delimiter
	private static final String RETURNTOKEN = "^[A-Za-z0-9]+\\r\\n$"; // the valid token pattern with return delimiter
	private static final String REGEXMESSAGE = "^[\\x20-\\x7E]*\\r\\n$"; // the message pattern with return delimiter
																			// for Response message

	private static Boolean noParamFlag = false; // to indicate the Request function contains params or not

	protected String function = "";
	protected CookieList cookieList = null; // the cookieList part in request message

	/**
	 * Creates a new G8R message by deserializing from the given input according to the specified serialization.
	 * @param in user input source
	 * @return new G8R message
	 * @throws G8R.serialization.ValidationException - if validation fails
	 * @throws java.io.IOException - if I/O problem
	 * @throws java.lang.NullPointerException - if in is null
	 */
	public static G8RMessage decode(MessageInput in) throws IOException, ValidationException {
		Objects.requireNonNull(in, "MessageInput is null");
		// check the head
		String head = in.getToken(SPACEDELIMITER);
		if (head.isEmpty()) {
			throw new EOFException("No valid message head found before the EOF of inputstream");
		}
		if (!HEAD.equals(head)) { // Invalid header
			throw new ValidationException("Invalid head", head);
		}
		// check the type
		String type = in.getToken(SPACEDELIMITER);
		if (REQUEST.equals(type)) { // the message type is request
			checkCommand(in); // only check the command part, not in use
			String function = getFunction(in, REQUEST); // get function
			String[] params = new String[0]; // to store the params
			if (!noParamFlag) { // if function contains params, get the params
				params = getParams(in);
			}
			CookieList cookieList = new CookieList(in); // get the cookielist
			return new G8RRequest(function, params, cookieList); // return a G8RRequest
		}
		/**/
		else if (RESPONSE.equals(type)) { // the message type is response
			String status = getStatus(in); // get status
			String function = getFunction(in, RESPONSE); // get function
			String message = getMessage(in); // get message
			CookieList cookieList = new CookieList(in); // get cookielist

			return new G8RResponse(status, function, message, cookieList); // return a G8RResponse
		}
		/**/
		else {
			throw new ValidationException("Invalid type", type); // Invalid message type
		}

	}

	/**
	 * Encode the entire G8R message
	 * @param out serialization output sink
	 * @throws java.io.IOException - if I/O problem
	 * @throws java.lang.NullPointerException - if out is null
	 */
	public void encode(MessageOutput out) throws java.io.IOException {

	}

	/**
	 * Returns string representation of function in request message. 
	 * @return  string representation of function in request message
	 */
	public String getFunction() {
		return function;

	}

	/**
	 * Set the function in request message. 
	 * @param function  the input function
	 * @throws ValidationException invalid function
	 * @throws NullPointerException - if null command
	 * 
	 */
	public void setFunction(String function) throws ValidationException {
		Objects.requireNonNull(function, "function is null");
		// check the function
		if (!isValidToken(function)) {
			throw new ValidationException("Invalid function", function);
		}
		this.function = function;
	}

	/**
	 * check whether the command is valid(should be "RUN").
	 * @param in the input message
	 */
	private static void checkCommand(MessageInput in) throws ValidationException, IOException {
		String cmd = in.getToken(SPACEDELIMITER);
		if (!COMMAND.equals(cmd)) {
			throw new ValidationException("Invalid command" + cmd, cmd);
		}
	}
    
	
	/**
	 * Returns cookielist in request message. 
	 * @return  cookielist in request message
	 */
	public CookieList getCookieList() {
		return new CookieList(cookieList);

	}

	/**
	 * Set the cookielist in request message. 
	 * the method is a hard copy method.
	 * @param cookieList the input cookieList
	 * @throws NullPointerException - if null cookie list
	 * 
	 */
	public void setCookieList(CookieList cookieList) {
		Objects.requireNonNull(cookieList, "cookieList is null");
		this.cookieList = new CookieList(cookieList);
	}
	
	
	/**
	 * get function part for both messages
	 * @param in the input message
	 * @param type the  message type
	 * @return the function string
	 * 
	 */
	private static String getFunction(MessageInput in, String type) throws ValidationException, IOException {
		String function = in.getToken(SPACEDELIMITER, RETURNDELIMITER); // get the token for function
		if (REQUEST.equals(type)) { // this is a request message function
			if (function.matches(SPACETOKEN)) { // the function is a function with params
				function = function.substring(0, function.length() - SPACEDELIMITER.length()); // remove the delimiter
				noParamFlag = false; // set the flag
			} else if (function.matches(RETURNTOKEN)) { // the function is a function with no params
				function = function.substring(0, function.length() - RETURNDELIMITER.length()); // remove the delimiter
				noParamFlag = true; // set the flag
			} else {
				throw new ValidationException("Invalid function" + function, function); // Otherwise, invalid function
			}
		} else if (RESPONSE.equals(type)) {// this is a response message function
			if (function.matches(SPACETOKEN)) { // valid token
				function = function.substring(0, function.length() - SPACEDELIMITER.length()); // remove the delimiter
			} else {
				throw new ValidationException("Invalid function" + function, function); // Otherwise, invalid function
			}
		}
		return function;
	}

	/**
	 * get params part for request message
	 * @param in the input message
	 * @return a string array contains the params
	 * 
	 */
	private static String[] getParams(MessageInput in) throws ValidationException, IOException {
		ArrayList<String> paramsList = new ArrayList<String>(); // to store all the params
		String param = ""; // to store on params
		while (!param.matches(RETURNTOKEN)) { // when get the end param, stop the loop
			param = in.getToken(SPACEDELIMITER, RETURNDELIMITER);
			if (param.matches(SPACETOKEN)) { // valid normal param
				paramsList.add(param.substring(0, param.length() - SPACEDELIMITER.length())); // remove delimiter
			} else if (param.matches(RETURNTOKEN)) { // valid end param
				paramsList.add(param.substring(0, param.length() - RETURNDELIMITER.length())); // remove delimiter
			} else {
				throw new ValidationException("Invalid param", param); // Invalid param
			}
		}
		return paramsList.toArray(new String[paramsList.size()]); // change the list to array and return the array
	}

	/**
	 * get status part for response message
	 * @param in the input message
	 * @return the status string
	 * 
	 */
	private static String getStatus(MessageInput in) throws ValidationException, IOException {
		String status = in.getToken(SPACEDELIMITER);

		if ("OK ".equals(status) || "ERROR ".equals(status)) {
			return status.substring(0, status.length() - SPACEDELIMITER.length()); // remove delimiter
		} else {
			throw new ValidationException("Invalid status", status);
		}
	}

	/**
	 * get message part for response message
	 * @param in the input message
	 * @return the message string
	 * 
	 */
	private static String getMessage(MessageInput in) throws IOException, ValidationException {
		String message = in.getToken(RETURNDELIMITER);
		if (message.length() < RETURNDELIMITER.length()) { // too short messages
			throw new ValidationException("Invalid message", message);
		}

		if (!message.matches(REGEXMESSAGE)) { // Invalid messages
			throw new ValidationException("Invalid message", message);
		}
		return message.substring(0, message.length() - RETURNDELIMITER.length()); // remove delimiter
	}

	/**
	 * check whether the token is valid(a sequence (length>=1) of alphanumeric characters).
	 * @param token token is the name or value for a cookie
	 * @return if the input token is a sequence (length>=1) of alphanumeric characters, return true. Otherwise false.
	 */
	protected static boolean isValidToken(String token) {
		String regex = "^[A-Za-z0-9]+$";
		return token.matches(regex);
	}

}
