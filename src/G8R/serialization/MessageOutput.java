/**
 * Class Name :     MessageOutput
 * Version    :     1.0
 * Author     :     Ruolin Wang
 * Date       :     01/17/2018
 * Function    :    This class contains the serialization output source for messages
 *                  
 *                                   
 * CopyRight Reserved
 * 
 */
package G8R.serialization;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;

/**
 * Serialization output source for messages
 */
public class MessageOutput {
	private OutputStream outputStream = null;
	private OutputStreamWriter outputStreamWriter = null;

	/**
	 * Constructs a new output source from an OutputStream
	 * 
	 * @param out byte input source
	 *            
	 * @exception java.lang.NullPointerException if out is null
	 *                
	 */
	public MessageOutput(java.io.OutputStream out) throws java.lang.NullPointerException {
		if (out == null) {
			throw new NullPointerException("OutputStream is null");
		}

		this.outputStream = out;
		this.outputStreamWriter = new OutputStreamWriter(out, StandardCharsets.US_ASCII);
	}

	/**
	 * Write the string to the outputStream
	 * 
	 * @param message the message that need to be written to the outputStream
	 * @throws IOException           
	 * 
	 */
	public void write(String message) throws IOException {
		outputStreamWriter.write(message);

	}

	/**
	 * Flush the data from the buffer to the real destination
	 * 
	 * @throws IOException           
	 * 
	 */
	public void flush() throws IOException {
		outputStreamWriter.flush();
	}

	/**
	 * Close the outputStreamWriter and outjputStream
	 * 
	 * @throws IOException           
	 * 
	 */
	public void close() throws IOException {
		outputStreamWriter.close();
		outputStream.close();
	}
}
