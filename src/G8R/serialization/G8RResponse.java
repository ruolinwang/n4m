package G8R.serialization;

import java.io.IOException;
import java.util.Objects;
import java.util.Set;

/**
 * @author Ruolin Wang
 *
 */
public class G8RResponse extends G8RMessage {

	private static final String SPACEDELIMITER = " ";
	private static final String RETURNDELIMITER = "\r\n";
	private static final String HEAD = "G8R/1.0 R ";
	private static final String REGEXMESSAGE = "^[\\x20-\\x7E]*$";

	private String status;
	private String message;

	/**
	 * Constructs G8R response using given values
	 * @param status response status
	 * @param function response function
	 * @param message response message
	 * @param cookies response cookie list
	 * @throws ValidationException if error with given values
	 * @throws NullPointerException if null parameter
	 */
	public G8RResponse(String status, String function, String message, CookieList cookies) throws ValidationException {
		setStatus(status);
		setFunction(function);
		setMessage(message);
		setCookieList(cookies);
	}

	/**
	 * Returns human-readable, complete (i.e., all attribute values), string representation of G8R response message
	 * The form is exactly the same pattern in specification 
	 * @return string representation of response message
	 */
	@Override
	public String toString() {
		String output = "G8R/1.0 R " + status + SPACEDELIMITER + function + SPACEDELIMITER + message + RETURNDELIMITER;
		Set<String> names = cookieList.getNames();

		for (String item : names) {
			output += item + "=" + cookieList.getValue(item) + RETURNDELIMITER;
		}

		output += RETURNDELIMITER;
		return output;

	}
	
	/**
     * Returns human-readable, complete (i.e., all attribute values), string representation of G8R response message
     * The form is a simple connection of all parts
     * @return string representation of response message
     */
	public String toSimpleString() {
        String output = "G8R/1.0 R " + status + SPACEDELIMITER + function + SPACEDELIMITER + message + SPACEDELIMITER;
        output = output += cookieList.toString();
        return output;
    }

	/**
	 * Return status
	 * @return status
	 */
	public String getStatus() {
		return status;

	}

	/**
	 * Set status
	 * @param status new status 
	 * @throws ValidationException if invalid status
	 * @throws NullPointerException if null status
	 */
	public void setStatus(String status) throws ValidationException {
		Objects.requireNonNull(status, "status is null");
		// check the status
		if (!"OK".equals(status) && !"ERROR".equals(status)) {
			throw new ValidationException("Invalid status", status);
		}
		this.status = status;
	}

	/**
	 * Return message
	 * @return message
	 */
	public String getMessage() {
		return message;

	}

	/**
	 * Set message
	 * @param message new message 
	 * @throws ValidationException if invalid message
	 * @throws NullPointerException if null message
	 */
	public void setMessage(String message) throws ValidationException {
		Objects.requireNonNull(message, "message is null");
		// check the message
		if (!message.matches(REGEXMESSAGE)) {
			throw new ValidationException("Invalid message", message);
		}
		this.message = message;
	}

	/**
	 * Encode the entire G8R response message
	 * @param out serialization output sink
	 * @throws IOException - if I/O problem
	 * @throws NullPointerException - if out is null
	 */
	@Override
	public void encode(MessageOutput out) throws java.io.IOException {
		out.write(HEAD + status + SPACEDELIMITER + function);
		out.write(SPACEDELIMITER + message);
		out.write(RETURNDELIMITER);
		out.flush();
		cookieList.encode(out);
	}

	/**
	 * Implement according to Object.hashCode()
	 * 
	 */
	@Override
	public int hashCode() {
		return function.hashCode() * 96 + status.hashCode() * 64 + message.hashCode() * 32 + cookieList.hashCode();
	}

	/**
	 * Implement according to Object.equals(java.lang.Object)
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if (obj == null || obj.getClass() != this.getClass()) {
			result = false;
		} else {
			G8RResponse response = (G8RResponse) obj;
			if (this.cookieList.equals(response.cookieList) && this.function.equals(response.function)
					&& this.status.equals(response.status) && this.message.equals(response.message)) {
				result = true;
			}

		}
		return result;
	}
}
