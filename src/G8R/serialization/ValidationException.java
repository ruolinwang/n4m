package G8R.serialization;

/**
 * Validation exception containing the token failing validation
 * @version 1.0
 */
public class ValidationException extends Exception {
	private String token;

	private static final long serialVersionUID = 1L;

	/**
	 * Constructs validation exception
	 * @param msg exception message
	 * @param token token causing validation failure
	 * @param cause exception cause
	 * @exception java.lang.NullPointerException throw NullPointerException
	 */
	public ValidationException(String msg, String token, Throwable cause) {
		super(msg, cause);
		if (token == null) {
			throw new NullPointerException("token is null" + msg);
		}
		this.token = token;
	}

	/**
	 * Constructs validation exception
	 * @param msg exception message
	 * @param token token causing validation failure
	 * @exception java.lang.NullPointerException throw NullPointerException
	 */
	public ValidationException(String msg, String token) {
		this(msg, token, null);
	}

	/**
	 * Return token
	 * @return token indicates which token has problem
	 *
	 */
	public String getToken() {
		return token;
	}

}
