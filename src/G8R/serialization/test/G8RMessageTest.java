/**
 * G8RMessageTest
 * Version 1.0
 * Author: Jian Cao, Ruolin Wang
 * Date : 01/29/2018
 * Copyright Reserved
 * 
 */
package G8R.serialization.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import G8R.serialization.CookieList;
import G8R.serialization.G8RMessage;
import G8R.serialization.G8RRequest;
import G8R.serialization.G8RResponse;
import G8R.serialization.MessageInput;
import G8R.serialization.MessageOutput;
import G8R.serialization.ValidationException;

/**
 * 
 * Class Name : G8RMessageTest
 * @author Ruolin Wang
 *
 */
public class G8RMessageTest {

	/**
	 * Totally simply test
	 */
	@DisplayName("G8R Message test")
	@Nested
	class Messagetest {
		@DisplayName("Valid Request Message")
		@ParameterizedTest()
		@ValueSource(strings = { "G8R/1.0 Q RUN FCN P1 P2\r\nx=1\r\ny=2\r\n\r\n" })
		/**
		 * test Valid Request Message
		 */
		void testValidRequestTest(String testStr) throws ValidationException, IOException {
			InputStream testIn = new ByteArrayInputStream(testStr.getBytes());
			MessageInput messageInput = new MessageInput(testIn);
			assertEquals(G8RMessage.decode(messageInput).toString(), "G8R/1.0 Q RUN FCN P1 P2\r\nx=1\r\ny=2\r\n\r\n");

		}

		@DisplayName("Valid Request Message no param")
		@ParameterizedTest()
		@ValueSource(strings = { "G8R/1.0 Q RUN FCN\r\nx=1\r\ny=2\r\n\r\n" })
		/**
		 * test Valid Request Message with no params
		 */
		void testValidRequestNoParamTest(String testStr) throws ValidationException, IOException {
			InputStream testIn = new ByteArrayInputStream(testStr.getBytes());
			MessageInput messageInput = new MessageInput(testIn);
			assertEquals(G8RMessage.decode(messageInput).toString(), "G8R/1.0 Q RUN FCN\r\nx=1\r\ny=2\r\n\r\n");

		}

		@DisplayName("Valid Response Message")
		@ParameterizedTest()
		@ValueSource(strings = { "G8R/1.0 R OK FCN Everything is \"ok\" \r\nx=1\r\ny=2\r\n\r\n",
				"G8R/1.0 R ERROR FCN Everything is \"ok\" \r\nx=1\r\ny=2\r\n\r\n" })
		/**
		 * test Valid Response Message
		 */
		void testValidResponseTest(String testStr) throws ValidationException, IOException {
			InputStream testIn = new ByteArrayInputStream(testStr.getBytes());
			MessageInput messageInput = new MessageInput(testIn);
			G8RMessage.decode(messageInput);
		}

		@DisplayName("Invalid Head")
		@ParameterizedTest()
		@ValueSource(strings = { "G8t/1.0 R OK FCN Everything is \"ok\" \r\nx=1\r\ny=2\r\n\r\n", "G8R/1.0 D" })
		/**
		 * test Message with invalid head
		 */
		void testErrorHeadTest(String testStr) throws ValidationException, IOException {
			InputStream testIn = new ByteArrayInputStream(testStr.getBytes());
			MessageInput messageInput = new MessageInput(testIn);
			assertThrows(ValidationException.class, () -> G8RMessage.decode(messageInput));

		}

		@DisplayName("Invalid Command")
		@ParameterizedTest()
		@ValueSource(strings = { "G8R/1.0 Q RU FCN P1 P2\r\nx=1\r\ny=2\r\n\r\n" })
		/**
		 * test Request Message with invalid command
		 */
		void testInvalidCommandTest(String testStr) throws ValidationException, IOException {
			InputStream testIn = new ByteArrayInputStream(testStr.getBytes());
			MessageInput messageInput = new MessageInput(testIn);
			assertThrows(ValidationException.class, () -> G8RMessage.decode(messageInput));

		}

		@DisplayName("Invalid Function")
		@ParameterizedTest()
		@ValueSource(strings = { "G8R/1.0 Q RUN F*CN P1 P2\r\nx=1\r\ny=2\r\n\r\n",
				"G8R/1.0 Q RUN FCN\r\n P1 P2\r\nx=1\r\ny=2\r\n\r\n",
				"G8R/1.0 R OK F*CN Everything is \"ok\" \r\nx=1\r\ny=2\r\n\r\n",
				"G8R/1.0 R OK FCN\r\n Everything is \"ok\" \r\nx=1\r\ny=2\r\n\r\n" })
		/**
		 * test R/Q Message with invalid function
		 */
		void testInvalidFuncMessageTest(String testStr) throws ValidationException, IOException {
			InputStream testIn = new ByteArrayInputStream(testStr.getBytes());
			MessageInput messageInput = new MessageInput(testIn);
			assertThrows(ValidationException.class, () -> G8RMessage.decode(messageInput));

		}

		@DisplayName("Invalid Params ")
		@ParameterizedTest()
		@ValueSource(strings = { "G8R/1.0 Q RUN FCN P**1 P2\r\nx=1\r\ny=2\r\n\r\n",
				"G8R/1.0 Q RUN FCN P1 P2 \r\nx=1\r\ny=2\r\n\r\n", "G8R/1.0 Q RUN FCN \r\nx=1\r\ny=2\r\n\r\n" })
		/**
		 * test Request Message with invalid params
		 */
		void testInvalidParamsMessageTest(String testStr) throws ValidationException, IOException {
			InputStream testIn = new ByteArrayInputStream(testStr.getBytes());
			MessageInput messageInput = new MessageInput(testIn);
			assertThrows(ValidationException.class, () -> G8RMessage.decode(messageInput));

		}

		@DisplayName("Invalid Cookies ")
		@ParameterizedTest()
		@ValueSource(strings = { "G8R/1.0 Q RUN FCN P1 P2\r\nx*=1\r\ny=2\r\n\r\n",
				"G8R/1.0 Q RUN FCN P1 P2\r\nx1\r\ny=2\r\n\r\n", "G8R/1.0 Q RUN FCN P1 P2\r\n=1\r\ny=2\r\n\r\n",
				"G8R/1.0 Q RUN FCN P1 P2\r\n1=\r\ny=2\r\n\r\n",
				"G8R/1.0 R OK F*CN Everything is \"ok\" \r\nx=\r\ny=2\r\n\r\n",
				"G8R/1.0 R OK F*CN Everything is \"ok\" \r\nx=1\r\ny=*2\r\n\r\n" })
		/**
		 * test messages with invalid cookies
		 */
		void testInvalidCookiesMessageTest(String testStr) throws ValidationException, IOException {
			InputStream testIn = new ByteArrayInputStream(testStr.getBytes());
			MessageInput messageInput = new MessageInput(testIn);
			assertThrows(ValidationException.class, () -> G8RMessage.decode(messageInput));

		}

		@DisplayName("Invalid Status ")
		@ParameterizedTest()
		@ValueSource(strings = { "G8R/1.0 R O FCN Everything is \"ok\" \r\nx=1\r\ny=2\r\n\r\n",
				"G8R/1.0 R OG FCN Everything is \"ok\" \r\nx=1\r\ny=2\r\n\r\n",
				"G8R/1.0 R OKt FCN Everything is \"ok\" \r\nx=1\r\ny=2\r\n\r\n",
				"G8R/1.0 R ER FCN Everything is \"ok\" \r\nx=1\r\ny=2\r\n\r\n",
				"G8R/1.0 R ERR FCN Everything is \"ok\" \r\nx=1\r\ny=2\r\n\r\n",
				"G8R/1.0 R ERRoR FCN Everything is \"ok\" \r\nx=1\r\ny=2\r\n\r\n",
				"G8R/1.0 R ERROR2 FCN Everything is \"ok\" \r\nx=1\r\ny=2\r\n\r\n" })
		/**
		 * test messages with Invalid Status
		 */
		void testInvalidStatusMessageTest(String testStr) throws ValidationException, IOException {
			InputStream testIn = new ByteArrayInputStream(testStr.getBytes());
			MessageInput messageInput = new MessageInput(testIn);
			assertThrows(ValidationException.class, () -> G8RMessage.decode(messageInput));

		}

		@DisplayName("Invalid Message ")
		@ParameterizedTest()
		@ValueSource(strings = { "G8R/1.0 R OK FCN Everything is\r \"ok\" \r\nx=1\r\ny=2\r\n\r\n",
				"G8R/1.0 R OK FCN Everything is", "G8R/1.0 R OK FCN E"

		})
		/**
		 * test Response message with invalid Message
		 */
		void testInvalidMessageTest(String testStr) throws ValidationException, IOException {
			InputStream testIn = new ByteArrayInputStream(testStr.getBytes());
			MessageInput messageInput = new MessageInput(testIn);
			assertThrows(ValidationException.class, () -> G8RMessage.decode(messageInput));
		}

		@DisplayName("EOF Test ")
		@ParameterizedTest()
		@ValueSource(strings = { "",

		})
		/**
		 * test EOF exception
		 */
		void testEOFMessageTest(String testStr) throws ValidationException, IOException {
			InputStream testIn = new ByteArrayInputStream(testStr.getBytes());
			MessageInput messageInput = new MessageInput(testIn);
			assertThrows(EOFException.class, () -> G8RMessage.decode(messageInput));

		}
	}

	/**
	 * Detailed G8RRequest test
	 */
	@DisplayName("G8RRequest test")
	@Nested
	class G8RRequestest {
		@DisplayName("G8RRequest setfunction() right ")
		@Test
		/**
		 * test setFunction() for G8RRequest 
		 * This is the right case
		 * 
		 */
		void testRequestSetFunctionRight() throws ValidationException, IOException {

			String testStr = "G8R/1.0 Q RUN aa 11 Xsadsad ccvvv sfdsf\r\na=1\r\nb=2\r\n\r\n";
			InputStream testIn;

			testIn = new ByteArrayInputStream(testStr.getBytes("ASCII"));
			MessageInput messageInput = new MessageInput(testIn);
			G8RMessage test = G8RMessage.decode(messageInput);
			if (test instanceof G8RRequest) {
				assertEquals("aa", ((G8RRequest) test).getFunction());
			} else {
				fail("fail");
			}
		}

		/**
		 * test setFunction() which input is null
		 */
		@DisplayName("G8RRequest setfunction() wrong nullpointer")
		@Test
		void testRequestSetFunctioNull() throws ValidationException, IOException {

			String testStr = "G8R/1.0 Q RUN aa 11 Xsadsad ccvvv sfdsf\r\na=1\r\nb=2\r\n\r\n";
			InputStream testIn;

			testIn = new ByteArrayInputStream(testStr.getBytes("ASCII"));
			MessageInput messageInput = new MessageInput(testIn);
			G8RMessage test = G8RMessage.decode(messageInput);
			if (test instanceof G8RRequest) {
				Throwable t = assertThrows(NullPointerException.class, () -> ((G8RRequest) test).setFunction(null));
				assertNotNull(t.getMessage());
			} else {
				fail("fail");
			}
		}

		/**
		 * test setFunction() which invalid function input
		 */
		@DisplayName("G8RRequest setfunction() wrong validtion")
		@Test
		void testRequestSetFunctioValidition() throws ValidationException, IOException {

			String testStr = "G8R/1.0 Q RUN aa 11 Xsadsad ccvvv sfdsf\r\na=1\r\nb=2\r\n\r\n";
			InputStream testIn;

			testIn = new ByteArrayInputStream(testStr.getBytes("ASCII"));
			MessageInput messageInput = new MessageInput(testIn);
			G8RMessage test = G8RMessage.decode(messageInput);
			if (test instanceof G8RRequest) {
				Throwable t = assertThrows(ValidationException.class, () -> ((G8RRequest) test).setFunction("a."));
				assertNotNull(t.getMessage());
			} else {
				fail("fail");
			}
		}

		/**
		 * test getCookieList() function
		 * This is the right case
		 */
		@DisplayName("G8RRequest getCookieList() test case")
		@Test
		void testRequestGetCookieList() throws ValidationException, IOException {

			String testStr = "G8R/1.0 Q RUN aa 11 Xsadsad ccvvv sfdsf\r\na=1\r\nb=2\r\n\r\n";
			InputStream testIn;

			testIn = new ByteArrayInputStream(testStr.getBytes("ASCII"));
			MessageInput messageInput = new MessageInput(testIn);
			G8RMessage test = G8RMessage.decode(messageInput);
			if (test instanceof G8RRequest) {
				CookieList testCookie = ((G8RRequest) test).getCookieList();
				assertEquals("Cookies=[a=1,b=2]", testCookie.toString());
			} else {
				fail("fail");
			}
		}

		/**
		 * test getparam() function
		 * This is the right case
		 */
		@DisplayName("G8RRequest getparam() test case")
		@Test
		void testRequestGetParamValidition() throws ValidationException, IOException {

			String testStr = "G8R/1.0 Q RUN aa 11 Xsadsad ccvvv sfdsf\r\na=1\r\nb=2\r\n\r\n";
			InputStream testIn;

			testIn = new ByteArrayInputStream(testStr.getBytes("ASCII"));
			MessageInput messageInput = new MessageInput(testIn);
			G8RMessage test = G8RMessage.decode(messageInput);
			if (test instanceof G8RRequest) {
				String[] paramStr = ((G8RRequest) test).getParams();

				assertNotEquals(paramStr, ((G8RRequest) test).getParams());
				paramStr = null;
				assertNotNull(((G8RRequest) test).getParams());

			} else {
				fail("fail");
			}
		}

		/**
		 * test setparam() function
		 * The input is null in one item of the array
		 * 
		 */
		@DisplayName("G8RRequest Setparam() NullPointerException")
		@Test
		void testRequestSetParamNull() throws ValidationException, IOException {

			String testStr = "G8R/1.0 Q RUN aa 11 Xsadsad ccvvv sfdsf\r\na=1\r\nb=2\r\n\r\n";
			InputStream testIn;
			String[] paramTest = new String[3];
			paramTest[0] = "abc";
			paramTest[1] = "c";
			testIn = new ByteArrayInputStream(testStr.getBytes("ASCII"));
			MessageInput messageInput = new MessageInput(testIn);
			G8RMessage test = G8RMessage.decode(messageInput);
			if (test instanceof G8RRequest) {
				Throwable t = assertThrows(NullPointerException.class, () -> ((G8RRequest) test).setParams(paramTest));
				assertNotNull(t.getMessage());
			} else {
				fail("fail");
			}
		}

		/**
		 * test setparam() function
		 * The input is invalid because of containing invalid characters
		 * 
		 */
		@DisplayName("G8RRequest Setparam() ValidationException")
		@Test
		void testRequestSetParamValid() throws ValidationException, IOException {

			String testStr = "G8R/1.0 Q RUN aa 11 Xsadsad ccvvv sfdsf\r\na=1\r\nb=2\r\n\r\n";
			InputStream testIn;
			String[] paramTest = new String[3];
			paramTest[0] = "abc";
			paramTest[1] = "c....";
			paramTest[2] = "xx";
			testIn = new ByteArrayInputStream(testStr.getBytes("ASCII"));
			MessageInput messageInput = new MessageInput(testIn);
			G8RMessage test = G8RMessage.decode(messageInput);
			if (test instanceof G8RRequest) {
				Throwable t = assertThrows(ValidationException.class, () -> ((G8RRequest) test).setParams(paramTest));
				assertNotNull(t.getMessage());
			} else {
				fail("fail");
			}
		}

		/**
		 * test setparam() function
		 * This test is to test we set the params by creating new space in memory
		 * not just get the reference
		 * We first set the params by input and then we change the input and compare params with input
		 * 
		 */
		@DisplayName("G8RRequest Setparam() Clear")
		@Test
		void testRequestSetParamClear() throws ValidationException, IOException {

			String testStr = "G8R/1.0 Q RUN aa 11 Xsadsad ccvvv sfdsf\r\na=1\r\nb=2\r\n\r\n";
			InputStream testIn;
			String[] paramTest = new String[3];
			paramTest[0] = "abc";
			paramTest[1] = "sdsd";
			paramTest[2] = "xx";
			testIn = new ByteArrayInputStream(testStr.getBytes("ASCII"));
			MessageInput messageInput = new MessageInput(testIn);
			G8RMessage test = G8RMessage.decode(messageInput);

			if (test instanceof G8RRequest) {
				((G8RRequest) test).setParams(paramTest);
				paramTest[1] = ".....";
				assertNotEquals(paramTest[1], ((G8RRequest) test).getParams()[1]);
			} else {
				fail("fail");
			}
		}

		/**
		 * test G8RRequest Encode() function
		 * This test is to test the encode function by encoding an original input 
		 * and then compare them
		 * Then we change set the params and encode again, compare the result
		 * 
		 */
		@DisplayName("G8RRequest Encode() ")
		@Test
		void testRequestEncode() throws ValidationException, IOException {

			String testStr = "G8R/1.0 Q RUN aa 11 Xsadsad ccvvv sfdsf\r\na=1\r\nb=2\r\n\r\n";
			InputStream testIn;
			String[] paramTest = new String[3];
			paramTest[0] = "abc";
			paramTest[1] = "sdsd";
			paramTest[2] = "xx";
			testIn = new ByteArrayInputStream(testStr.getBytes("ASCII"));
			MessageInput messageInput = new MessageInput(testIn);
			G8RMessage test = G8RMessage.decode(messageInput);
			OutputStream out = new ByteArrayOutputStream();
			MessageOutput messageOutput = new MessageOutput(out);
			if (test instanceof G8RRequest) {
				test.encode(messageOutput);
				String data = out.toString();
				assertEquals(testStr, data);
				if (test instanceof G8RRequest) {
					((G8RRequest) test).setParams(paramTest);
					OutputStream outModify = new ByteArrayOutputStream();
					MessageOutput msgOutModify = new MessageOutput(outModify);
					test.encode(msgOutModify);
					data = outModify.toString();

					assertEquals("G8R/1.0 Q RUN aa abc sdsd xx\r\na=1\r\nb=2\r\n\r\n", data);

				} else {
					fail("fail");
				}

			} else {
				fail("fail");
			}
		}
		/**
		 * test G8RRequest Tostring() function
		 * This is the right case
		 * 
		 */
		@DisplayName("G8RRequest Tostring() test case")
		@Test
		void testRequestTostring() throws ValidationException, IOException {

			String testStr = "G8R/1.0 Q RUN aa 11 Xsadsad ccvvv sfdsf\r\na=1\r\nb=2\r\n\r\n";
			InputStream testIn;

			testIn = new ByteArrayInputStream(testStr.getBytes("ASCII"));
			MessageInput messageInput = new MessageInput(testIn);
			G8RMessage test = G8RMessage.decode(messageInput);
			if (test instanceof G8RRequest) {
				assertEquals("G8R/1.0 Q RUN aa 11 Xsadsad ccvvv sfdsf\r\na=1\r\nb=2\r\n\r\n", ((G8RRequest) test).toString());
			} else {
				fail("fail");
			}
		}
		
		/**
		 * test G8RRequest Tostring() function with no cookies
		 * This is the right case
		 * 
		 */
		@DisplayName("G8RRequest Tostring() test case with no cookies")
		@Test
		void testRequestTostringWithNoParams() throws ValidationException, IOException {

			String testStr = "G8R/1.0 Q RUN aa 11 Xsadsad ccvvv sfdsf\r\n\r\n";
			InputStream testIn;

			testIn = new ByteArrayInputStream(testStr.getBytes("ASCII"));
			MessageInput messageInput = new MessageInput(testIn);
			G8RMessage test = G8RMessage.decode(messageInput);
			if (test instanceof G8RRequest) {
				assertEquals("G8R/1.0 Q RUN aa 11 Xsadsad ccvvv sfdsf\r\n\r\n", ((G8RRequest) test).toString());
			} else {
				fail("fail");
			}
		}

		/**
		 * test the situation that validation happens when using the Constructor with input parameter
		 * MessageInput in.
		 * 
		 *
		 */
		@DisplayName("G8RRequest(MessageInput) wrong ")
		@ParameterizedTest(name = "{index} => str=''{0}''")
		@ValueSource(strings = { "G8R/1.0 Q RUN  11\r\na=1\r\nb=2\r\n\r\n",
				"G8R/1.0 Q RUN a....a11Xsadsadccvvvsfdsf\r\na=1\r\nb=2\r\n\r\n",
				"G8R/1.0 Q RUN a ....a11Xsadsadccvvvsfdsf\r\na=1\r\nb=2\r\n\r\n", "\r\na=1\r\nb=2\r\n\r\n", "\r\n",
				"G8R/1.0 Q RUN a ....a11Xsadsadccvvvsfdsf\r\na=1\r\nb=2\r\n\r\n",
				"G8R/1.0 XX RUN a ....a11Xsadsadccvvvsfdsf\r\na=1\r\nb=2\r\n\r\n", "G11 ", "G11  ",
				"G8R/1.0 Q VVVVVV555!!! a ....a11Xsadsadccvvvsfdsf\r\na=1\r\nb=2\r\n\r\n",
				"G8R/1.0 Q RUN a AD..SA ..a11Xsadsadccvvvsfdsf\r\na=1\r\nb=2\r\n\r\n",
				"G8R/1.0 R XXX aa asdasdasadsadax0\r\n\r\n", "G8R/1.0 R OK ... asdasdasadsadax0\r\n\r\n", "G8R/1.0 ",
				"G8R/1.0 R ", "G8R/1.0 R OK ", "G8R/1.0 R OK ad ", "G8R/1.0 Q " })
		void testDecodeWrong(String testStr) throws ValidationException, IOException {

			InputStream testIn;

			testIn = new ByteArrayInputStream(testStr.getBytes("ASCII"));
			MessageInput messageInput = new MessageInput(testIn);

			Throwable t = assertThrows(ValidationException.class, () -> G8RMessage.decode(messageInput));
			assertNotNull(t.getMessage());

		}

		/**
		 * test the situation that validation happens when using the Constructor with input parameter
		 * MessageInput in.
		 * 
		 * This is the right cases
		
		 */
		@DisplayName("G8RRequest(MessageInput) RIGHT ")
		@ParameterizedTest(name = "{index} => str=''{0}''")
		@ValueSource(strings = { "G8R/1.0 Q RUN f\r\na=1\r\nb=2\r\n\r\n",

		})
		void testDecodeRight(String testStr) throws ValidationException, IOException {

			InputStream testIn;

			testIn = new ByteArrayInputStream(testStr.getBytes("ASCII"));
			MessageInput messageInput = new MessageInput(testIn);
			G8RMessage test = G8RMessage.decode(messageInput);
			if (test instanceof G8RRequest) {

				OutputStream outModify = new ByteArrayOutputStream();
				MessageOutput msgOutModify = new MessageOutput(outModify);
				test.encode(msgOutModify);
				String data = outModify.toString();

				assertEquals(testStr, data);

			} else {

			}

		}

		/**
		 * test the situation that validation happens when using the Constructor with input parameter
		 * MessageInput in.
		 * 
		 * The input String is enmpty
		
		 */
		@DisplayName("G8RRequest(MessageInput) empty string ")
		@ParameterizedTest()
		@ValueSource(strings = { "" })
		void testDecodeWrongEmpty(String testStr) throws ValidationException, IOException {

			InputStream testIn;

			testIn = new ByteArrayInputStream(testStr.getBytes("ASCII"));
			MessageInput messageInput = new MessageInput(testIn);
			Throwable t = assertThrows(EOFException.class, () -> G8RMessage.decode(messageInput));
			assertNotNull(t.getMessage());

		}
        
		/**
		 * Test the G8RMessage.decode method to create a G8RRequest
		 * This is the right case
		 *
		 */
		@Test
		void testRequest() throws ValidationException, IOException {

			String testStr = "G8R/1.0 Q RUN aa 11 Xsadsad ccvvv sfdsf\r\na=1\r\nb=2\r\n\r\n";
			InputStream testIn = new ByteArrayInputStream(testStr.getBytes("ASCII"));
			MessageInput messageInput = new MessageInput(testIn);
			G8RMessage temp = G8RMessage.decode(messageInput);
			OutputStream out = new ByteArrayOutputStream();
			MessageOutput messageOutput = new MessageOutput(out);
			if (temp instanceof G8RRequest) {
				temp.encode(messageOutput);
				String data = out.toString();
				assertEquals(testStr, data);
			} else {
				fail("testrequest");
			}
		}

	}
    
	/**
	 * Detailed G8RResponse test
	 */
	@DisplayName("G8RResponse test")
	@Nested
	class G8RResponsetest {
		@DisplayName("getCookieList() test case")
		@Test
		void testGetCookieList() throws ValidationException, IOException {

			String testStr = "G8R/1.0 R OK aa asdasdasadsadax0\r\n\r\na=1\r\nb=2\r\n\r\n";
			InputStream testIn;

			testIn = new ByteArrayInputStream(testStr.getBytes("ASCII"));
			MessageInput messageInput = new MessageInput(testIn);
			G8RMessage test = G8RMessage.decode(messageInput);
			if (test instanceof G8RResponse) {
				CookieList testCookie = ((G8RResponse) test).getCookieList();
				assertEquals("Cookies=[]", testCookie.toString());
			} else {
				fail("fail");
			}
		}
		/**
		 * Detailed G8RResponse test
		 */
		@DisplayName(" Encode() ")
		@Test
		/**
		 * test G8RResponse Encode() function
		 * This is a right case
		 */
		void testResponseEncode() throws ValidationException, IOException {

			String testStr = "G8R/1.0 R OK aa asdasdasadsadax0\r\na=1\r\nb=2\r\n\r\n";
			InputStream testIn;

			testIn = new ByteArrayInputStream(testStr.getBytes("ASCII"));
			MessageInput messageInput = new MessageInput(testIn);
			G8RMessage test = G8RMessage.decode(messageInput);
			OutputStream out = new ByteArrayOutputStream();
			MessageOutput messageOutput = new MessageOutput(out);
			if (test instanceof G8RResponse) {
				test.encode(messageOutput);
				String data = out.toString();
				assertEquals(testStr, data);
			} else {
				fail("fail");
			}
		}

		/**
		 * test G8RResponse created by G8RMessage.decode(MessageInput)
		 * This is a right case
		 */
		@DisplayName("G8RResponse decode (MessageInput) right ")
		@Test
		void testDecodeRight() throws ValidationException, IOException {
			char tt = 0x7e;
			String teString = "G8R/1.0 R OK aa asdasdasadsadax0" + tt + "\r\n\r\n";

			InputStream testIn = new ByteArrayInputStream(teString.getBytes("ASCII"));
			MessageInput messageInput = new MessageInput(testIn);
			G8RMessage temp = G8RMessage.decode(messageInput);
			OutputStream out = new ByteArrayOutputStream();
			MessageOutput messageOutput = new MessageOutput(out);
			if (temp instanceof G8RResponse) {
				temp.encode(messageOutput);
				String data = out.toString();
				assertEquals(teString, data);
			} else {
				fail("testDecodeRight");
			}
		}
        
		/**
		 * test G8RResponse created by G8RMessage.decode(MessageInput)
		 * with input containing non printable characters
		 */
		@DisplayName("G8RResponse decode(MessageInput) NonPrintable ")
		@Test
		void testDecodeNonPrintable() throws ValidationException, IOException {
			char tt = 0x7f;
			String teString = "G8R/1.0 R OK aa asdasdasadsadax0" + tt + "\r\n\r\n";

			InputStream testIn = new ByteArrayInputStream(teString.getBytes("ASCII"));
			MessageInput messageInput = new MessageInput(testIn);

			Throwable t = assertThrows(ValidationException.class, () -> G8RMessage.decode(messageInput));
			assertNotNull(t.getMessage());
		}
        
		/**
		 * test G8RResponse setFunction()
		 * with input that is null
		 */
		@DisplayName("G8RResponse setfunction() wrong nullpointer")
		@Test
		void testResponseSetFunctioNull() throws ValidationException, IOException {

			String testStr = "G8R/1.0 R OK aa asdasdasadsadax0\r\n\r\n";
			InputStream testIn;

			testIn = new ByteArrayInputStream(testStr.getBytes("ASCII"));
			MessageInput messageInput = new MessageInput(testIn);
			G8RMessage test = G8RMessage.decode(messageInput);
			if (test instanceof G8RResponse) {
				Throwable t = assertThrows(NullPointerException.class, () -> ((G8RResponse) test).setFunction(null));
				assertNotNull(t.getMessage());
			} else {
				fail("fail");
			}
		}
        
		/**
		 * test G8RResponse setStatus()
		 * with invalid input which is like "error"
		 */
		@DisplayName("G8RResponse setStatus() validation")
		@ParameterizedTest(name = "{index} => str=''{0}''")
		@ValueSource(strings = { "G8R/1.0 R OK aa asdasdasadsadax0\r\n\r\n",
				"G8R/1.0 R ERROR aa asdasdasadsadax0\r\n\r\n" })
		void testSetStatusValidok(String testStr) throws ValidationException, IOException {

			InputStream testIn = new ByteArrayInputStream(testStr.getBytes("ASCII"));
			MessageInput messageInput = new MessageInput(testIn);
			G8RMessage test = G8RMessage.decode(messageInput);
			if (test instanceof G8RResponse) {
				Throwable t = assertThrows(ValidationException.class, () -> ((G8RResponse) test).setStatus("error"));
				assertNotNull(t.getMessage());

			} else {
				fail("fail");
			}
		}
       
		/**
		 * test G8RResponse setStatus()
		 * with invalid input which is like "ok"
		 */
		@DisplayName("G8RResponse setStatus() validation")
		@ParameterizedTest(name = "{index} => str=''{0}''")
		@ValueSource(strings = { "G8R/1.0 R OK aa asdasdasadsadax0\r\n\r\n",
				"G8R/1.0 R ERROR aa asdasdasadsadax0\r\n\r\n" })
		void testSetStatusValiderror(String testStr) throws ValidationException, IOException {

			InputStream testIn = new ByteArrayInputStream(testStr.getBytes("ASCII"));
			MessageInput messageInput = new MessageInput(testIn);
			G8RMessage test = G8RMessage.decode(messageInput);
			if (test instanceof G8RResponse) {
				Throwable t = assertThrows(ValidationException.class, () -> ((G8RResponse) test).setStatus("ok"));
				assertNotNull(t.getMessage());

			} else {
				fail("fail");
			}
		}
        
		/**
		 * test G8RResponse getStatus()
		 * This is a right test case
		 */
		@DisplayName("G8RResponse getStatus() valid")
		@Test
		void testResponseGetStatusNull() throws ValidationException, IOException {

			String testStr = "G8R/1.0 R OK aa asdasdasadsadax0\r\n\r\n";
			InputStream testIn;

			testIn = new ByteArrayInputStream(testStr.getBytes("ASCII"));
			MessageInput messageInput = new MessageInput(testIn);
			G8RMessage test = G8RMessage.decode(messageInput);
			if (test instanceof G8RResponse) {
				assertEquals("OK", ((G8RResponse) test).getStatus());

			} else {
				fail("fail");
			}
		}
		
		/**
		 * test G8RResponse setFunction()
		 * With invalid input
		 */
		@DisplayName("G8RResponse setfunction() validation")
		@Test
		void testResponseSetFunctionValid() throws ValidationException, IOException {

			String testStr = "G8R/1.0 R OK aa asdasdasadsadax0\r\n\r\n";
			InputStream testIn;

			testIn = new ByteArrayInputStream(testStr.getBytes("ASCII"));
			MessageInput messageInput = new MessageInput(testIn);
			G8RMessage test = G8RMessage.decode(messageInput);
			if (test instanceof G8RResponse) {
				Throwable t = assertThrows(ValidationException.class, () -> ((G8RResponse) test).setFunction("@@@@"));
				assertNotNull(t.getMessage());
			} else {
				fail("fail");
			}
		}
        
		/**
		 * test G8RResponse setMessage()
		 * With invalid input
		 */
		@DisplayName("G8RResponse setMessage() validation")
		@Test
		void testResponseSetMessageValid() throws ValidationException, IOException {

			String testStr = "G8R/1.0 R OK aa asdasdasadsadax0\r\n\r\n";
			InputStream testIn;

			testIn = new ByteArrayInputStream(testStr.getBytes("ASCII"));
			MessageInput messageInput = new MessageInput(testIn);
			G8RMessage test = G8RMessage.decode(messageInput);
			if (test instanceof G8RResponse) {
				Throwable t = assertThrows(ValidationException.class, () -> ((G8RResponse) test).setMessage("\n\r"));
				assertNotNull(t.getMessage());
			} else {
				fail("fail");
			}
		}

		/**
		 * test G8RResponse Tostring()
		 */
		@DisplayName("G8RResponse Tostring() test case")
		@Test
		void testTostring() throws ValidationException, IOException {

			String testStr = "G8R/1.0 R OK aa asdasdasadsadax0\r\na=1\r\nb=2\r\n\r\n";
			InputStream testIn;

			testIn = new ByteArrayInputStream(testStr.getBytes("ASCII"));
			MessageInput messageInput = new MessageInput(testIn);
			G8RMessage test = G8RMessage.decode(messageInput);
			if (test instanceof G8RResponse) {
				assertAll("String",
						() -> assertEquals("G8R/1.0 R OK aa asdasdasadsadax0\r\na=1\r\nb=2\r\n\r\n",
								((G8RResponse) test).toString()),
						() -> assertEquals("asdasdasadsadax0", ((G8RResponse) test).getMessage()),
						() -> assertEquals("aa", ((G8RResponse) test).getFunction()));

			} else {
				fail("fail");
			}
		}

	}
}
